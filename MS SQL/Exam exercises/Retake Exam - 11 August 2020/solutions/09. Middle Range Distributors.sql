SELECT  d.Name DistributorName
	  , i.Name IngredientName
	  , p.Name ProductName
	  , AVG(f.Rate) AverageRate
FROM Distributors d
	JOIN Ingredients i ON i.DistributorId = d.Id
	JOIN ProductsIngredients pin ON pin.IngredientId = i.Id
	JOIN Products p ON pin.ProductId = p.Id
	JOIN Feedbacks f ON f.ProductId = p.Id
GROUP BY p.Name, d.Name, i.Name
HAVING AVG(f.Rate) BETWEEN 5 AND 8
ORDER BY d.Name, i.Name, p.Name