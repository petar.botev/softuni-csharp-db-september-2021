ALTER TRIGGER Trig
ON Products
INSTEAD OF DELETE
AS
	DECLARE @ind INT 
	SET @ind = (SELECT TOP 1 * FROM deleted)

	DELETE Feedbacks
	 WHERE ProductId = @ind
	 
	DELETE ProductsIngredients
	 WHERE ProductId = @ind

	 DELETE Products
	 WHERE Id = @ind
GO

DELETE FROM Products WHERE Id = 7

	

