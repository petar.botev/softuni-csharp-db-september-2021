SELECT dsa.CountryName, dsa.DisributorName
FROM(
SELECT c.Name CountryName
	 , dist.Name DisributorName
	 --, MAX(dist.IngId) MaxIng
	 , dist.IngId
	 , DENSE_RANK() OVER(PARTITION BY c.Name ORDER BY dist.IngId DESC) Ranking
FROM Countries c
	 JOIN (SELECT d.Name, COUNT(i.Id) IngId, d.Id, d.CountryId
          FROM Distributors d
          	LEFT JOIN Ingredients i ON i.DistributorId = d.Id
          GROUP BY d.Name, d.Id, d.CountryId) dist ON dist.CountryId = c.Id
		  ) dsa
WHERE dsa.Ranking = 1
ORDER BY CountryName, DisributorName

--------------

SELECT d.Id, d.Name, c.Name, i.Name, 
FROM Countries c
	LEFT JOIN Distributors d ON c.Id = d.CountryId
	LEFT JOIN Ingredients i ON i.DistributorId = d.Id
GROUP BY d.Id, d.Name, c.Name, i.Name 

---------------
SELECT d.Name, SUM(i.Id)
FROM Distributors d
	JOIN Ingredients i ON i.DistributorId = d.Id
GROUP BY d.Name

SELECT *
FROM Countries c 
	LEFT JOIN Distributors d ON d.CountryId = c.Id
	LEFT JOIN Ingredients i ON i.DistributorId = d.Id
	ORDER BY c.Name


SELECT *
FROM Countries