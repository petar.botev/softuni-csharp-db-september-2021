SELECT FirstName,	Age,	PhoneNumber
FROM Customers cu
JOIN Countries co ON cu.CountryId = co.Id
WHERE (Age >= 21 AND cu.FirstName LIKE '%an%') OR (PhoneNumber LIKE '%38' AND co.Name != 'Greece')
ORDER BY FirstName, Age DESC