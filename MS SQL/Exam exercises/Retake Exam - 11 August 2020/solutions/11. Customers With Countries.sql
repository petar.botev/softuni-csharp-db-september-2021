CREATE VIEW v_UserWithCountries
AS
SELECT CONCAT(cu.FirstName, ' ', cu.LastName) CustomerName
	,	cu.Age,	cu.Gender
	,	co.Name CountryName
FROM Customers cu
	JOIN Countries co ON co.Id = cu.CountryId

	SELECT TOP 5 *
  FROM v_UserWithCountries
 ORDER BY Age
