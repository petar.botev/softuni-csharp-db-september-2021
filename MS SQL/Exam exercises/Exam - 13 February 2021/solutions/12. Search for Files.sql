CREATE PROC usp_SearchForFiles(@fileExtension VARCHAR(50))
AS
SELECT Id,	Name, CONCAT(Size,'KB')	Size
FROM Files
WHERE Name LIKE '%' + @fileExtension
ORDER BY Id, Name, Size DESC

EXEC usp_SearchForFiles 'txt'