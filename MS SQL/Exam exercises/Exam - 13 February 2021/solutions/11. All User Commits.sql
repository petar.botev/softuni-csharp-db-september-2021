CREATE FUNCTION udf_AllUserCommits(@username VARCHAR(30))
RETURNS INT
AS
BEGIN
DECLARE @res INT 
SET @res = (SELECT COUNT(c.ContributorId) [Count]
			FROM Commits c 
			RIGHT JOIN Users u ON c.ContributorId = u.Id
			WHERE u.Username = @username
			)
	RETURN @res
END

SELECT dbo.udf_AllUserCommits('UnderSinduxrein')

--second solution
CREATE FUNCTION udf_AllUserCommits(@username VARCHAR(30))
RETURNS INT
AS
BEGIN
		DECLARE @count INT
		SET @count = (SELECT COUNT(c.ContributorId) AS [Count]
		FROM [Users] AS u
		LEFT JOIN [Commits] AS c ON c.ContributorId = u.Id
		WHERE u.[Username] = @username
		) 
 
		RETURN @count 
END