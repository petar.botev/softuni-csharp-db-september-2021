--my solution

SELECT f.Id, f.Name, CONCAT(f.Size,'KB') Size
FROM Files f
LEFT JOIN Files fp ON f.Id = fp.ParentId
WHERE fp.ParentId IS NULL
ORDER BY f.Id, f.Name, f.Size DESC

--Solution 2

SELECT Id, [Name], CONCAT(Size, 'KB') AS Size
    FROM Files f
      WHERE
       (SELECT TOP 1 ParentId 
         FROM Files 
         WHERE ParentId = f.Id) IS NULL
 ORDER BY Id

