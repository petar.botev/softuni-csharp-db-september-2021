
SELECT TOP 5 r.Id,	r.Name,	COUNT(*) Commits
FROM Repositories r
LEFT JOIN Commits c ON c.RepositoryId = r.Id
LEFT JOIN RepositoriesContributors rc ON r.Id = rc.RepositoryId
GROUP BY r.Id,r.Name
ORDER BY Commits DESC, r.Id, r.Name

--secind solution
SELECT TOP(5) r.Id,
              r.[Name],
              COUNT(c.Id)
         FROM Users AS u
         JOIN RepositoriesContributors AS rc ON u.Id = rc.ContributorId
         JOIN Repositories AS r ON rc.RepositoryId = r.Id
         JOIN Commits As c ON r.Id =c.RepositoryId
     GROUP BY r.Id, r.[Name]
     ORDER BY COUNT(c.Id) DESC, r.Id, r.[Name]