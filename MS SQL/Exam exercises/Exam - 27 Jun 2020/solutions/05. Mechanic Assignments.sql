SELECT CONCAT(m.FirstName, ' ', m.LastName) Mechanic, j.Status, j.IssueDate
FROM Mechanics m
JOIN Jobs j ON j.MechanicId = m.MechanicId
ORDER BY m.MechanicId, IssueDate, JobId
