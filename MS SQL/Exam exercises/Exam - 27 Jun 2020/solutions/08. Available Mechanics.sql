SELECT COUNT(t.Status)
FROM(
SELECT CONCAT(m.FirstName, ' ', m.LastName) FullName, j.Status
FROM Mechanics m
LEFT JOIN Jobs j ON m.MechanicId = j.MechanicId
GROUP BY CONCAT(m.FirstName, ' ', m.LastName), j.Status) AS t
GROUP BY t.FullName



SELECT COUNT(t.Status)
FROM(
SELECT CONCAT(m.FirstName, ' ', m.LastName) FullName, j.Status
FROM Mechanics m
LEFT JOIN Jobs j ON m.MechanicId = j.MechanicId
WHERE j.Status NOT IN ('In Progress')
GROUP BY CONCAT(m.FirstName, ' ', m.LastName), j.Status) AS t
GROUP BY t.FullName

SELECT t.FullName
FROM(
SELECT CONCAT(m.FirstName, ' ', m.LastName) FullName, j.Status, m.MechanicId
FROM Mechanics m
LEFT JOIN Jobs j ON m.MechanicId = j.MechanicId
GROUP BY CONCAT(m.FirstName, ' ', m.LastName), j.Status,m.MechanicId) AS t
GROUP BY t.FullName,t.MechanicId
HAVING COUNT(t.Status) = 1
ORDER BY t.MechanicId

SELECT CONCAT(m.FirstName, ' ', m.LastName) Available
FROM Jobs j
LEFT JOIN Mechanics m ON j.MechanicId = m.MechanicId
WHERE j.Status = 'Finished' OR j.Status IS NULL
GROUP BY CONCAT(m.FirstName, ' ', m.LastName), m.MechanicId
ORDER BY m.MechanicId

-- working solution
SELECT CONCAT(FirstName, ' ', LastName) AS Available
FROM Mechanics
WHERE MechanicId NOT IN (
							SELECT MechanicId, Status FROM Jobs
							WHERE Status = 'In Progress'
							GROUP BY MechanicId, Status
						)
