SELECT j.JobId, SUM(p.Price) Total
FROM Jobs j
JOIN PartsNeeded pn ON pn.JobId = j.JobId
JOIN Parts p ON pn.PartId = p.PartId
WHERE j.Status = 'Finished'
GROUP BY j.JobId
ORDER BY SUM(p.Price) DESC, j.JobId