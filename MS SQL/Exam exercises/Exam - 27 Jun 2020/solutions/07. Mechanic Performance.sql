SELECT CONCAT(m.FirstName, ' ', m.LastName) AS Mechanic, AVG(DATEDIFF(DAY, j.IssueDate, j.FinishDate)) AS [Average Days]
FROM Mechanics m
JOIN Jobs j ON m.MechanicId = j.MechanicId
WHERE FinishDate IS NOT NULL
GROUP BY CONCAT(m.FirstName, ' ', m.LastName), m.MechanicId
ORDER BY m.MechanicId