SELECT CONCAT(c.FirstName, ' ', c.LastName) Client
, DATEDIFF(DAY, j.IssueDate, CONVERT(DATE, '2017-04-24', 23)) [Days going]
, Status
FROM Clients c
JOIN Jobs j ON j.ClientId = c.ClientId
WHERE j.Status != 'Finished'
ORDER BY [Days going] DESC, c.ClientId
