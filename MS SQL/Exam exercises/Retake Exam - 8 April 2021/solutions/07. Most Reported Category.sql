SELECT TOP(5) c.Name CategoryName, COUNT(r.Description) ReportsNumber
FROM Categories c
JOIN Reports r ON c.Id = r.CategoryId
GROUP BY c.Name
ORDER BY COUNT(r.Description) DESC, c.Name