SELECT [Description], CONVERT(varchar, OpenDate, 105) OpenDate
FROM Reports
WHERE EmployeeId IS NULL
ORDER BY CONVERT(date, OpenDate, 105), Description

