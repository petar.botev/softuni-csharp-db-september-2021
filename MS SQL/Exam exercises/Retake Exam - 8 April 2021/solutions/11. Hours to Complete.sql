CREATE FUNCTION udf_HoursToComplete(@StartDate DATETIME, @EndDate DATETIME)
RETURNS INT
BEGIN
	IF(@StartDate IS NULL OR @EndDate IS NULL)
		RETURN 0
	DECLARE @result INT
	SET @result = DATEDIFF(HOUR, @StartDate, @EndDate)
	RETURN @result
END
