SELECT DISTINCT CONCAT(e.FirstName, ' ', e.LastName) FullName, COUNT(u.Username) UserCount
FROM Employees e
LEFT JOIN Reports r ON e.Id = r.EmployeeId
LEFT JOIN Users u ON r.UserId = u.Id
GROUP BY e.FirstName, e.LastName
ORDER BY COUNT(u.Username) DESC, CONCAT(e.FirstName, ' ', e.LastName)