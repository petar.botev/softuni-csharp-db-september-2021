SELECT ISNULL(NULLIF(CONCAT(e.FirstName, ' ', LastName), ''), 'None') Employee
, ISNULL(d.Name, 'None') Department
, ISNULL(c.Name, 'None') Category
, ISNULL(Description, 'None') Description 
, ISNULL(CONVERT(varchar, OpenDate, 104), 'None') OpenDate
, ISNULL(Label, 'None') Status
, ISNULL(u.Name, 'None') [User]  
FROM Reports r
 LEFT JOIN Employees As e ON r.EmployeeId = e.Id
    LEFT JOIN Categories AS c ON r.CategoryId = c.Id
    LEFT JOIN Departments AS d ON e.DepartmentId = d.Id
    LEFT JOIN [Status] AS s ON r.StatusId = s.Id
    LEFT JOIN Users AS u ON r.UserId = u.Id
ORDER BY FirstName DESC
, LastName DESC
, d.Name
, c.Name
, r.Description
, CONVERT(date, OpenDate, 104)
, s.Label
, u.Name
