SELECT Username, c.Name CategoryName
FROM Users u
JOIN Reports r ON r.UserId = u.Id
JOIN Categories c ON r.CategoryId = c.Id
WHERE DAY(u.Birthdate) = DAY(r.OpenDate)
ORDER BY u.Username, c.Name