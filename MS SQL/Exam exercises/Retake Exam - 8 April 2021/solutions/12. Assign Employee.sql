CREATE PROC usp_AssignEmployeeToReport(@EmployeeId INT, @ReportId INT)
AS
DECLARE @reportSelect INT
DECLARE @employeeSelect INT

SET @reportSelect = (
SELECT DepartmentId 
	FROM Reports r
	JOIN Categories c ON r.CategoryId = c.Id
	JOIN Departments d ON c.DepartmentId = d.Id
	WHERE r.Id = @ReportId
)

SET @employeeSelect = (
SELECT DepartmentId
	FROM Employees e
	LEFT JOIN Departments d ON e.DepartmentId = d.Id
	WHERE e.Id = @EmployeeId
	)

	IF (@reportSelect = @employeeSelect)
	BEGIN
		UPDATE Reports
		SET EmployeeId = @EmployeeId
		WHERE Id = @ReportId
	END
	ELSE
		THROW 50001, 'Employee doesn''t belong to the appropriate department!', 1
	

	EXEC dbo.usp_AssignEmployeeToReport 17,2
