CREATE FUNCTION udf_GetColonistsCount(@PlanetName NVARCHAR (30)) 
RETURNS INT
AS
BEGIN

DECLARE @result INT = (SELECT COUNT(c.Id)
					   FROM Planets p
					   LEFT JOIN Spaceports sr ON sr.PlanetId = p.Id
					   LEFT JOIN Journeys j ON j.DestinationSpaceportId = sr.Id
					   LEFT JOIN TravelCards tc ON tc.JourneyId = j.Id
					   LEFT JOIN Colonists c ON c.Id = tc.ColonistId
					   WHERE p.Name = @PlanetName
					   )

RETURN @result
END
GO

SELECT dbo.udf_GetColonistsCount('Otroyphus')


SELECT p.Id, p.Name, *--, COUNT(c.Id)
FROM Planets p
JOIN Spaceports sr ON sr.PlanetId = p.Id
JOIN Journeys j ON j.DestinationSpaceportId = sr.Id
JOIN TravelCards tc ON tc.JourneyId = j.Id
JOIN Colonists c ON c.Id = tc.ColonistId
--WHERE p.Id = 15
WHERE c.Id IS NULL
GROUP BY p.Id, p.Name