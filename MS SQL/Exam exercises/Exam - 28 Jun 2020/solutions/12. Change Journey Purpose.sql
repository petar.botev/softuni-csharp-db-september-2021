CREATE PROC usp_ChangeJourneyPurpose(@JourneyId INT, @NewPurpose NVARCHAR(11))
AS
DECLARE @myJourneyId INT = (SELECT Id
						  FROM Journeys
						  WHERE Id = @JourneyId
						  )

DECLARE @journeyPurpose NVARCHAR(11) = (SELECT Purpose
										FROM Journeys
										WHERE Id = @JourneyId
										)
IF(@myJourneyId IS NULL)
	THROW 50001, 'The journey does not exist!', 1

IF(@journeyPurpose = @NewPurpose)
	THROW 50002, 'You cannot change the purpose!', 2

UPDATE Journeys
SET Purpose = @NewPurpose
WHERE Id = @JourneyId