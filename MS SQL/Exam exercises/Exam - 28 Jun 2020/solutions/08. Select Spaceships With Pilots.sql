SELECT s.Name, s.Manufacturer
FROM Spaceships s
JOIN Journeys j ON j.SpaceshipId = s.Id
JOIN TravelCards tc ON tc.JourneyId = j.Id
JOIN Colonists c ON c.Id = tc.ColonistId
WHERE (c.BirthDate > '1989-01-01') AND (tc.JobDuringJourney = 'Pilot')
ORDER BY s.Name