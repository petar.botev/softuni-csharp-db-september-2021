CREATE FUNCTION udf_GetAvailableRoom(@HotelId INT, @Date DATETIME2, @People INT)
RETURNS VARCHAR(100)
AS
BEGIN
DECLARE @room TABLE (
						roomId INT
						, roomType NVARCHAR(20)
						, beds INT
						, total DECIMAL(18,2)
					)

INSERT INTO @room (roomId, roomType, beds, total)
SELECT TOP(1) r.Id, r.Type, r.Beds, ((h.BaseRate + r.Price) * @People) Total
FROM Rooms r
JOIN Trips t ON T.RoomId = r.Id
JOIN Hotels h ON h.Id = r.HotelId
WHERE ((@Date NOT BETWEEN t.ArrivalDate AND t.ReturnDate) OR (t.CancelDate IS NOT NULL)) AND r.HotelId = @HotelId
AND r.Beds > @People
GROUP BY r.Id, r.Type, r.Beds, h.BaseRate, r.Price
ORDER BY Total DESC

IF((SELECT roomId FROM @room) IS NULL)
	RETURN 'No rooms available'
								
DECLARE @roomId INT = (SELECT roomId FROM @room)
DECLARE @roomType NVARCHAR(20) = (SELECT roomType FROM @room)
DECLARE @beds INT = (SELECT beds FROM @room)
DECLARE @total DECIMAL(18,2) = (SELECT total FROM @room)
				
RETURN CONCAT('Room ', @roomId, ': ', @roomType, ' (', @beds, ' beds', ') - $', @total);

END

GO
 
 SELECT dbo.udf_GetAvailableRoom(112, '2011-12-17', 2)
 SELECT dbo.udf_GetAvailableRoom(94, '2015-07-26', 3)

SELECT TOP(1) r.Id, r.Type, r.Beds, ((h.BaseRate + r.Price) * 3) Total, t.CancelDate, t.ArrivalDate, t.ReturnDate, h.Id
FROM Rooms r
JOIN Trips t ON T.RoomId = r.Id
JOIN Hotels h ON h.Id = r.HotelId
WHERE (('2015-07-26' NOT BETWEEN t.ArrivalDate AND t.ReturnDate) OR (t.CancelDate IS NOT NULL)) AND r.HotelId = 94
AND r.Beds > 3
GROUP BY r.Id, r.Type, r.Beds, h.BaseRate, r.Price, t.CancelDate, t.ArrivalDate, t.ReturnDate, h.Id
ORDER BY Total DESC