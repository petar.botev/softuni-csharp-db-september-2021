SELECT a.Id, a.Email, c.Name City, COUNT(t.Id) Trips
FROM Accounts a
	JOIN AccountsTrips atr ON atr.AccountId = a.Id
	JOIN Trips t ON atr.TripId = t.Id
	JOIN Rooms r ON r.Id = t.RoomId
	JOIN Hotels h ON r.HotelId = h.Id
	JOIN Cities c ON c.Id = h.CityId
WHERE a.CityId = c.Id
GROUP BY a.Id, a.Email, c.Name
ORDER BY Trips DESC, a.Id