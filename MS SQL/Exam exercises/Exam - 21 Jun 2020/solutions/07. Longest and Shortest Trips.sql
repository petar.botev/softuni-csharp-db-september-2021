SELECT a.Id
	, CONCAT(a.FirstName, ' ', a.LastName)	FullName
	,	MAX(DATEDIFF(DAY, t.ArrivalDate, t.ReturnDate)) LongestTrip
	,	MIN(DATEDIFF(DAY, t.ArrivalDate, t.ReturnDate)) ShortestTrip
FROM Accounts a
	 JOIN AccountsTrips act ON act.AccountId = a.Id
	 JOIN Trips t ON t.Id = act.TripId
WHERE a.MiddleName IS NULL AND t.CancelDate IS NULL
GROUP BY a.Id, a.FirstName, a.LastName
ORDER BY LongestTrip DESC, ShortestTrip