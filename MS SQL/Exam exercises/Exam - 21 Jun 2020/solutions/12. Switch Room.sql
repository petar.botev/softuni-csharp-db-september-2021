CREATE PROC usp_SwitchRoom(@TripId INT, @TargetRoomId INT)
AS

DECLARE @tripRoomHotelId INT = (SELECT r.HotelId
						   FROM Trips t
						   JOIN Rooms r ON t.RoomId = r.Id
						   WHERE t.Id = @TripId)

DECLARE @targetRoomHotelId INT = (SELECT HotelId
						   FROM Rooms
						   WHERE Id = @TargetRoomId)

DECLARE @targetRoomBeds INT = (SELECT Beds
							   FROM Rooms
							   WHERE Id = @TargetRoomId)

DECLARE @tripsAccounts INT = (SELECT COUNT(a.Id)
							  FROM Trips t
							  JOIN AccountsTrips act ON act.TripId = t.Id
							  JOIN Accounts a ON act.AccountId = a.Id
							  WHERE t.Id = @TripId
							  GROUP BY t.Id)

IF(@tripRoomHotelId != @targetRoomHotelId)
	THROW 51001, 'Target room is in another hotel!', 1;

IF(@targetRoomBeds < @tripsAccounts)
	THROW 51002, 'Not enough beds in target room!', 2;

UPDATE Trips
SET RoomId = @TargetRoomId
WHERE Id = @TripId


EXEC dbo.usp_SwitchRoom 10, 7
EXEC dbo.usp_SwitchRoom 10, 8
EXEC dbo.usp_SwitchRoom 10, 11

SELECT RoomId FROM Trips WHERE Id = 10