SELECT FirstName,	LastName, FORMAT(BirthDate, 'MM-dd-yyyy') BirthDate,	c.Name Hometown,	Email
FROM Accounts a
JOIN Cities c ON c.Id = a.CityId
WHERE Email LIKE 'e%'
ORDER BY Hometown