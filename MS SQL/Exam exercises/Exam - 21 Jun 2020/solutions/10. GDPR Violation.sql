SELECT t.Id
	, CASE
		WHEN a.MiddleName IS NULL THEN CONCAT(a.FirstName, ' ', LastName)
		ELSE CONCAT(a.FirstName, ' ', a.MiddleName, ' ', LastName)	
	END [Full Name]
	, c.Name	[From]
	, c2.Name	[To]
	,	CASE
			WHEN t.CancelDate IS NOT NULL THEN 'Canceled'
			ELSE CAST(DATEDIFF(DAY, ArrivalDate, ReturnDate) AS varchar) + ' days'
		END Duration
	--, t.CancelDate
FROM Trips t
	JOIN AccountsTrips act ON act.TripId = t.Id
	JOIN Accounts a ON a.Id = act.AccountId
	JOIN Cities c ON c.Id = a.CityId
	JOIN Rooms r ON r.Id = t.RoomId
	JOIN Hotels h ON h.Id = r.HotelId
	JOIN Cities c2 ON c2.Id = h.CityId
ORDER BY [Full Name], t.Id
	