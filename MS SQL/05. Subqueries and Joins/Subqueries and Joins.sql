-- 01. Employee Address

SELECT TOP(5) e.EmployeeID, e.JobTitle, a.AddressID,a.AddressText
FROM Employees e
JOIN Addresses a ON e.AddressID = a.AddressID
ORDER BY a.AddressID

-- 02. Addresses with Towns

SELECT TOP(50) e.FirstName, e.LastName, t.Name Town, a.AddressText
FROM Towns t
JOIN Addresses a ON t.TownID = a.TownID 
JOIN Employees e ON a.AddressID = e.AddressID
ORDER BY e.FirstName, e.LastName

-- 03. Sales Employees

SELECT e.EmployeeID, e.FirstName, e.LastName, d.Name AS DepartmentName
FROM Employees e
JOIN Departments d ON e.DepartmentID = d.DepartmentID
WHERE d.Name = 'Sales'
ORDER BY e.EmployeeID
------------
SELECT e.EmployeeID, e.FirstName, e.LastName, d.Name AS DepartmentName
FROM Employees e
JOIN Departments d ON e.DepartmentID = d.DepartmentID AND d.Name = 'Sales'
ORDER BY e.EmployeeID

-- 04. Employee Departments

SELECT TOP(5) e.EmployeeID, e.FirstName, e.Salary, d.Name AS DepartmentName
FROM Employees e
JOIN Departments d ON e.DepartmentID = d.DepartmentID
WHERE e.Salary > 15000
ORDER BY d.DepartmentID

-- 05. Employees Without Projects

SELECT TOP(3) e.EmployeeID, e.FirstName
FROM EmployeesProjects ep
RIGHT JOIN Employees e ON ep.EmployeeID = e.EmployeeID
LEFT JOIN Projects p ON ep.ProjectID = p.ProjectID
WHERE p.ProjectID IS NULL
ORDER BY e.EmployeeID

-- 06. Employees Hired After

SELECT e.FirstName, e.LastName, e.HireDate, d.Name AS DeptName
FROM Employees e
JOIN Departments d ON e.DepartmentID = d.DepartmentID
WHERE YEAR(e.HireDate) > 1999 AND d.Name IN ('Sales', 'Finance')
ORDER BY e.HireDate

-- 07. Employees With Project

SELECT TOP(5) e.EmployeeID, e.FirstName, p.Name AS ProjectName
FROM Projects p
JOIN EmployeesProjects ep ON p.ProjectID = ep.ProjectID
JOIN Employees e ON ep.EmployeeID = e.EmployeeID
WHERE p.StartDate > CONVERT(datetime, '13.02.2002', 104)
AND p.EndDate IS NULL
ORDER BY e.EmployeeID

-- 08. Employee 24

SELECT e.EmployeeID
     , e.FirstName
	 , CASE
		WHEN YEAR(p.StartDate) >= 2005 THEN NULL
	   ELSE p.Name
	   END AS ProjectName
FROM Projects p
JOIN EmployeesProjects ep ON p.ProjectID = ep.ProjectID
JOIN Employees e ON ep.EmployeeID = e.EmployeeID
WHERE e.EmployeeID = 24

-- 09. Employee Manager

SELECT e.EmployeeID, e.FirstName, e.ManagerID, m.FirstName AS ManagerName
FROM Employees e
JOIN Employees m ON e.ManagerID = m.EmployeeID
WHERE e.ManagerID IN(3,7)
ORDER BY e.EmployeeID

-- 10. Employees Summary

SELECT TOP(50) e.EmployeeID
       , CONCAT(e.FirstName, ' ',e.LastName) AS EmployeeName
       , CONCAT(m.FirstName, ' ',m.LastName) AS ManagerName
       , d.Name AS DepartmentName
FROM Employees e
JOIN Employees m ON e.ManagerID = m.EmployeeID
JOIN Departments d ON e.DepartmentID = d.DepartmentID
ORDER BY e.EmployeeID

-- 11. Min Average Salary
SELECT MIN(av) AS MinAverageSalary
FROM
(SELECT AVG(e.Salary) AS av
FROM Employees e
JOIN Departments d ON e.DepartmentID = d.DepartmentID
GROUP BY d.DepartmentID) AS aaa

-- 12. Highest Peaks in Bulgaria

SELECT c.CountryCode, m.MountainRange, p.PeakName, p.Elevation
FROM MountainsCountries mc
JOIN Mountains m ON mc.MountainId = m.Id
JOIN Countries c ON mc.CountryCode = c.CountryCode
JOIN Peaks p ON m.Id = p.MountainId
WHERE p.Elevation > 2835 AND c.CountryName = 'Bulgaria'
ORDER BY p.Elevation DESC

-- 13. Count Mountain Ranges

SELECT c.CountryCode, COUNT(co.ContinentCode)AS MountainRanges
FROM MountainsCountries mc
JOIN Mountains m ON mc.MountainId = m.Id
JOIN Countries c ON mc.CountryCode = c.CountryCode
JOIN Continents co ON c.ContinentCode = co.ContinentCode 
WHERE c.CountryName IN ('Bulgaria', 'Russia', 'United States')
GROUP BY c.CountryCode

-- 14. Countries With or Without Rivers

SELECT TOP(5) c.CountryName, r.RiverName
FROM Countries c
LEFT JOIN CountriesRivers cr ON c.CountryCode = cr.CountryCode
LEFT JOIN Rivers r ON cr.RiverId = r.Id
JOIN Continents co ON c.ContinentCode = co.ContinentCode
WHERE co.ContinentName = 'Africa'
ORDER BY c.CountryName 

-- 15. Continents and Currencies (not included in final score)

SELECT ContinentCode, CurrencyCode, Total
FROM
(SELECT ContinentCode, CurrencyCode, COUNT(CurrencyCode) AS Total
,DENSE_RANK() OVER(PARTITION BY ContinentCode ORDER BY COUNT(CurrencyCode) DESC) 
AS Ranked
FROM Countries
GROUP BY ContinentCode, CurrencyCode) AS j
WHERE Ranked = 1 AND Total > 1
ORDER BY ContinentCode 

-- 16. Countries Without any Mountains

SELECT COUNT(c.CountryName) AS Count
FROM Countries c
LEFT JOIN MountainsCountries mc ON c.CountryCode = mc.CountryCode
WHERE mc.MountainId IS NULL


-- 17. Highest Peak and Longest River by Country

SELECT TOP(5) c.CountryName
, MAX(p.Elevation) AS HighestPeakElevation
, MAX(r.Length) AS LongestRiverLength
FROM Countries c
LEFT JOIN CountriesRivers cr ON c.CountryCode = cr.CountryCode
LEFT JOIN MountainsCountries mc ON c.CountryCode = mc.CountryCode
LEFT JOIN Rivers r ON cr.RiverId = r.Id
LEFT JOIN Mountains m ON mc.MountainId = m.Id
LEFT JOIN Peaks p ON p.MountainId = m.Id
GROUP BY c.CountryName
ORDER BY HighestPeakElevation DESC, LongestRiverLength DESC, c.CountryName
