SELECT c.LastName, AVG(s.Length) CiagrLength, CEILING(AVG(s.RingRange)) CiagrRingRange
FROM Clients c
JOIN ClientsCigars cc ON cc.ClientId = c.Id
JOIN Cigars ci ON ci.Id = cc.CigarId
JOIN Sizes s ON s.Id = ci.SizeId
GROUP BY c.LastName
ORDER BY CiagrLength DESC
