SELECT c.Id, CONCAT(FirstName, ' ', LastName) ClientName, c.Email
FROM Clients c
LEFT JOIN ClientsCigars cc ON c.Id = cc.ClientId
LEFT JOIN Cigars ci ON ci.Id = cc.CigarId
WHERE ci.Id IS NULL
ORDER BY ClientName