CREATE FUNCTION udf_ClientWithCigars(@name VARCHAR(30))
RETURNS INT
AS
BEGIN
DECLARE @result INT = (SELECT COUNT(cc.CigarId)
						FROM Clients c
						JOIN ClientsCigars cc ON cc.ClientId = c.Id
						WHERE c.FirstName = @name)

RETURN @result
END