CREATE PROCEDURE usp_SearchByTaste(@taste VARCHAR(50))
AS
SELECT c.CigarName
	, '$' + CAST(c.PriceForSingleCigar AS VARCHAR(20)) Price
	, t.TasteType
	, b.BrandName
	, CAST(s.[Length] AS VARCHAR(10)) + ' cm' CigarLength
	, CAST(s.RingRange AS VARCHAR(10)) + ' cm' CigarRingRange
FROM Cigars c
LEFT JOIN Brands b ON b.Id = c.BrandId
LEFT JOIN Sizes s ON s.Id = c.SizeId
LEFT JOIN Tastes t ON t.Id = c.TastId
WHERE t.TasteType  = @taste
ORDER BY CigarLength, CigarRingRange DESC
