SELECT CONCAT(c.FirstName, ' ', c.LastName) FullName
	,	Country
	,	ZIP
	, '$' + CAST(MAX(ci.PriceForSingleCigar) AS VARCHAR(50)) CigarPrice
FROM Clients c
JOIN ClientsCigars cc ON c.Id = cc.ClientId
JOIN Cigars ci ON ci.Id = cc.CigarId
JOIN Addresses a ON a.Id = c.AddressId
WHERE TRY_PARSE(a.ZIP AS INT) IS NOT NULL
GROUP BY CONCAT(c.FirstName, ' ', c.LastName),	Country,	ZIP
ORDER BY FullName