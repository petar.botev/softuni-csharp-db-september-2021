-- 01. Records� Count

SELECT COUNT(*) [Count]
FROM WizzardDeposits

-- 02. Longest Magic Wand

SELECT MAX(MagicWandSize) LongestMagicWand
FROM WizzardDeposits

-- 03. Longest Magic Wand per Deposit Groups

SELECT DepositGroup, MAX(MagicWandSize) LongestMagicWand
FROM WizzardDeposits
GROUP BY DepositGroup

-- 04. Smallest Deposit Group per Magic Wand Size (not included in final score)

SELECT TOP(2) DepositGroup
FROM WizzardDeposits
GROUP BY DepositGroup
ORDER BY AVG(MagicWandSize)

-- 05. Deposits Sum

SELECT DepositGroup, SUM(DepositAmount)
FROM WizzardDeposits
GROUP BY DepositGroup

-- 06. Deposits Sum for Ollivander Family

SELECT DepositGroup, SUM(DepositAmount)
FROM WizzardDeposits
WHERE MagicWandCreator = 'Ollivander family'
GROUP BY DepositGroup

-- 07. Deposits Filter

SELECT DepositGroup, SUM(DepositAmount) TotalSum
FROM WizzardDeposits
WHERE MagicWandCreator = 'Ollivander family'
GROUP BY DepositGroup
HAVING SUM(DepositAmount) < 150000
ORDER BY TotalSum DESC

-- 08. Deposit Charge

SELECT DepositGroup, MagicWandCreator, MIN(DepositCharge) MinDepositCharge
FROM WizzardDeposits
GROUP BY DepositGroup, MagicWandCreator
ORDER BY MagicWandCreator, DepositGroup

-- 09. Age Groups

SELECT 
CASE
  WHEN Age BETWEEN 0 AND 10 THEN '[0-10]'
  WHEN Age BETWEEN 11 AND 20 THEN '[11-20]'
  WHEN Age BETWEEN 21 AND 30 THEN '[21-30]'
  WHEN Age BETWEEN 31 AND 40 THEN '[31-40]'
  WHEN Age BETWEEN 41 AND 50 THEN '[41-50]'
  WHEN Age BETWEEN 51 AND 60 THEN '[51-60]'
  WHEN Age > 60 THEN '[61+]'
END AS AgeGroup
,COUNT(Age) WizardCount
FROM WizzardDeposits
GROUP BY 
CASE
  WHEN Age BETWEEN 0 AND 10 THEN '[0-10]'
  WHEN Age BETWEEN 11 AND 20 THEN '[11-20]'
  WHEN Age BETWEEN 21 AND 30 THEN '[21-30]'
  WHEN Age BETWEEN 31 AND 40 THEN '[31-40]'
  WHEN Age BETWEEN 41 AND 50 THEN '[41-50]'
  WHEN Age BETWEEN 51 AND 60 THEN '[51-60]'
  WHEN Age > 60 THEN '[61+]'
END 

-- 10. First Letter

SELECT LEFT(FirstName, 1)
FROM WizzardDeposits
WHERE DepositGroup = 'Troll Chest'
GROUP BY LEFT(FirstName, 1)

-- 11. Average Interest

SELECT DepositGroup, IsDepositExpired, AVG(DepositInterest)
FROM WizzardDeposits
WHERE DepositStartDate > CONVERT(datetime,'01/01/1985', 103)
GROUP BY DepositGroup, IsDepositExpired
ORDER BY DepositGroup DESC, IsDepositExpired

-- 12. Rich Wizard, Poor Wizard (not included in final score)

SELECT SUM([Difference]) AS SumDifference
FROM
(SELECT w.FirstName, w.DepositAmount 
,w.DepositAmount - a.DepositAmount AS [Difference]
FROM WizzardDeposits w
LEFT JOIN (SELECT FirstName, DepositAmount, ROW_NUMBER() OVER(ORDER BY Id) AS rowNumber
FROM WizzardDeposits
WHERE Id > 1) AS a ON w.Id = a.rowNumber) AS tr 

-- 13. Departments Total Salaries

SELECT DepartmentID, SUM(Salary) TotalSalary
FROM Employees
GROUP BY DepartmentID

-- 14. Employees Minimum Salaries

SELECT DepartmentID, MIN(Salary) MinimumSalary
FROM Employees
WHERE DepartmentID IN(2,5,7) AND HireDate > CONVERT(datetime,'01/01/2000', 103)
GROUP BY DepartmentID 

-- 15. Employees Average Salaries

SELECT * INTO NewTable
FROM Employees
WHERE Salary > 30000

DELETE FROM NewTable
WHERE ManagerID = 42

UPDATE NewTable
SET Salary = Salary + 5000
WHERE DepartmentID = 1

SELECT DepartmentID, AVG(Salary) AverageSalary
FROM NewTable
GROUP BY DepartmentID

-- 16. Employees Maximum Salaries

SELECT DepartmentID, MAX(Salary) MaxSalary
FROM Employees
GROUP BY DepartmentID
HAVING MAX(Salary) NOT BETWEEN 30000 AND 70000

-- 17. Employees Count Salaries

SELECT COUNT(Salary) Count
FROM Employees
WHERE ManagerID IS NULL

-- 18. 3rd Highest Salary (not included in final score)

SELECT DepartmentID, Salary AS ThirdHighestSalary
FROM
(SELECT DepartmentID, Salary, 
DENSE_RANK() OVER (PARTITION BY DepartmentID ORDER BY Salary DESC) AS r
FROM Employees
GROUP BY DepartmentID, Salary) AS t
WHERE r = 3

-- 19. Salary Challenge (not included in final score)

SELECT TOP(10) e.FirstName, e.LastName, e.DepartmentID
FROM Employees e
JOIN 
(SELECT DepartmentID, AVG(Salary) AS s 
FROM Employees
GROUP BY DepartmentID) a ON e.DepartmentID = a.DepartmentID
WHERE e.Salary > a.s
ORDER BY e.DepartmentID
