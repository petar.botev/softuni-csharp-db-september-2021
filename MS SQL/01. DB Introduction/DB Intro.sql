-- Problem 1.	Create Database

CREATE DATABASE Minions

-- Problem 2.	Create Tables

CREATE TABLE Minions (
Id INT NOT NULL, 
[Name] VARCHAR(30), 
Age TINYINT)

ALTER TABLE Minions
ADD CONSTRAINT PK_Minions PRIMARY KEY (Id)

CREATE TABLE Towns (
Id INT NOT NULL, 
[Name] VARCHAR(30))

ALTER TABLE Towns
ADD CONSTRAINT PK_Towns PRIMARY KEY (Id)

-- Problem 3.	Alter Minions Table

ALTER TABLE Minions 
ADD TownId INT

ALTER TABLE Minions
ADD CONSTRAINT FK_Town FOREIGN KEY (TownId) REFERENCES Towns(Id)

-- Problem 4.	Insert Records in Both Tables

INSERT INTO Towns (Id, [Name])
VALUES (1, 'Sofia')
INSERT INTO Towns (Id, [Name])
VALUES (2, 'Plovdiv')
INSERT INTO Towns (Id, [Name])
VALUES (3, 'Varna')

INSERT INTO Minions(Id, [Name], Age, TownId)
VALUES (1, 'Kevin', 22, 1)
INSERT INTO Minions(Id, [Name], Age, TownId)
VALUES (2, 'Bob', 15, 3)
INSERT INTO Minions(Id, [Name], Age, TownId)
VALUES (3, 'Steward', NULL, 2)

-- Problem 5.	Truncate Table Minions

TRUNCATE TABLE Minions;

-- Problem 6.	Drop All Tables

DROP TABLE Minions
DROP TABLE Towns

-- Problem 7. Create Table People

CREATE TABLE People (
Id INT PRIMARY KEY IDENTITY NOT NULL,
[Name] NVARCHAR(200) NOT NULL,
Picture VARBINARY(MAX),
Height DECIMAL(18, 2),
[Weight] DECIMAL(18, 2),
Gender CHAR(1) NOT NULL,
Birthdate DATE NOT NULL,
Biography NVARCHAR(MAX)) 


SET IDENTITY_INSERT People OFF

INSERT INTO People(Id, [Name], Picture, Height, [Weight], Gender, Birthdate, Biography)
VALUES(1, 'Gosho', 21, 2.1, 3.1, 'f', NULL, 'bla')
INSERT INTO People(Id, [Name], Picture, Height, [Weight], Gender, Birthdate, Biography)
VALUES(2, 'Gosho', 21, 2.1, 3.1, 'f', '2000-01-01', 'bla')
INSERT INTO People(Id, [Name], Picture, Height, [Weight], Gender, Birthdate, Biography)
VALUES(3, 'Gosho', 21, 2.1, 3.1, 'f', '2000-01-01', 'bla')
INSERT INTO People(Id, [Name], Picture, Height, [Weight], Gender, Birthdate, Biography)
VALUES(4, 'Gosho', 21, 2.1, 3.1, 'f', '2000-01-01', 'bla')
INSERT INTO People(Id, [Name], Picture, Height, [Weight], Gender, Birthdate, Biography)
VALUES(5, 'Gosho', 21, 2.1, 3.1, 'f', '2000-01-01', 'bla')



-- Problem 8.	Create Table Users

CREATE TABLE Users (
Id BIGINT IDENTITY NOT NULL,
Username VARCHAR(30) UNIQUE NOT NULL,
[Password] VARCHAR(26) NOT NULL,
ProfilePicture VARBINARY(900),
LastLoginTime DATETIME,
IsDeleted VARCHAR(5))

ALTER TABLE Users
ADD CONSTRAINT PK_Users PRIMARY KEY (Id)

SET IDENTITY_INSERT Users ON

INSERT INTO Users (Id, Username, [Password], ProfilePicture, LastLoginTime, IsDeleted)
VALUES(1, 'Gosho', 'asdasd', 21, '', 'true')
INSERT INTO Users (Id, Username, [Password], ProfilePicture, LastLoginTime, IsDeleted)
VALUES(2, 'pesho', 'asdasd', 21, '2000-01-01', 'true')
INSERT INTO Users (Id, Username, [Password], ProfilePicture, LastLoginTime, IsDeleted)
VALUES(3, 'ivan', 'asdasd', 21, '2000-01-01', 'true')
INSERT INTO Users (Id, Username, [Password], ProfilePicture, LastLoginTime, IsDeleted)
VALUES(4, 'dragn', 'asdasd', 21, '2000-01-01', 'true')
INSERT INTO Users (Id, Username, [Password], ProfilePicture, LastLoginTime, IsDeleted)
VALUES(5, 'kocio', 'asdasd', 21, '2000-01-01', 'true')

TRUNCATE TABLE Users

-- Problem 9.	Change Primary Key

ALTER TABLE Users
DROP CONSTRAINT PK_Users;

ALTER TABLE Users
ADD CONSTRAINT PK_Users PRIMARY KEY (Id, Username)

-- Problem 10.	Add Check Constraint

ALTER TABLE Users
ADD CONSTRAINT CHK_PasswordLength CHECK (LEN(Password) > 5);

-- Problem 11.	Set Default Value of a Field

ALTER TABLE Users
ADD CONSTRAINT df_Date
DEFAULT SYSDATETIME() FOR LastLoginTime;

ALTER TABLE Users
DROP CONSTRAINT df_Date

-- Problem 12.	Set Unique Field

ALTER TABLE Users
DROP CONSTRAINT PK_Users

ALTER TABLE Users
ADD CONSTRAINT PK_Users PRIMARY KEY (Id)

ALTER TABLE Users
ADD UNIQUE (Username)

-- Problem 13.	Movies Database
CREATE DATABASE Movies

CREATE TABLE Directors (
Id INT IDENTITY NOT NULL, 
DirectorName VARCHAR(50) NOT NULL, 
Notes VARCHAR(MAX))

CREATE TABLE Genres (
Id INT IDENTITY NOT NULL, 
GenreName VARCHAR(50) NOT NULL, 
Notes VARCHAR(MAX))

CREATE TABLE Categories  (
Id INT IDENTITY NOT NULL, 
CategoryName VARCHAR(50) NOT NULL, 
Notes VARCHAR(MAX))

CREATE TABLE Movies (
Id INT IDENTITY NOT NULL, 
Title VARCHAR(50) NOT NULL, 
DirectorId INT,
CopyrightYear DATETIME2,
[Length] DECIMAL(18, 2),
GenreId INT,
CategoryId INT,
Rating TINYINT,
Notes VARCHAR(MAX))

ALTER TABLE Directors
ADD CONSTRAINT PK_Directors PRIMARY KEY (Id)

ALTER TABLE Genres
ADD CONSTRAINT PK_Genres PRIMARY KEY (Id)

ALTER TABLE Categories
ADD CONSTRAINT PK_Categories PRIMARY KEY (Id)

ALTER TABLE Movies
ADD CONSTRAINT PK_Movies PRIMARY KEY (Id)

SET IDENTITY_INSERT Movies ON

INSERT INTO Movies (Id, Title, DirectorId, CopyrightYear, [Length], GenreId, CategoryId, Rating, Notes)
VALUES(1, 'title', 1, '2000-01-01', 2.1, 1, 1, 5, 'bla')
INSERT INTO Movies (Id, Title, DirectorId, CopyrightYear, [Length], GenreId, CategoryId, Rating, Notes)
VALUES(2, 'title', 1, '2000-01-01', 2.1, 1, 1, 5, 'bla')
INSERT INTO Movies (Id, Title, DirectorId, CopyrightYear, [Length], GenreId, CategoryId, Rating, Notes)
VALUES(3, 'title', 1, '2000-01-01', 2.1, 1, 1, 5, 'bla')
INSERT INTO Movies (Id, Title, DirectorId, CopyrightYear, [Length], GenreId, CategoryId, Rating, Notes)
VALUES(4, 'title', 1, '2000-01-01', 2.1, 1, 1, 5, 'bla')
INSERT INTO Movies (Id, Title, DirectorId, CopyrightYear, [Length], GenreId, CategoryId, Rating, Notes)
VALUES(5, 'title', 1, '2000-01-01', 2.1, 1, 1, 5, 'bla')

SET IDENTITY_INSERT Movies OFF

SET IDENTITY_INSERT Categories ON

INSERT INTO Categories (Id,CategoryName, Notes)
VALUES(1, 'Gosho', 'asdasd')
INSERT INTO Categories (Id, CategoryName, Notes)
VALUES(2, 'Gosho', 'asdasd')
INSERT INTO Categories (Id, CategoryName, Notes)
VALUES(3, 'Gosho', 'asdasd')
INSERT INTO Categories (Id, CategoryName, Notes)
VALUES(4, 'Gosho', 'asdasd')
INSERT INTO Categories (Id, CategoryName, Notes)
VALUES(5, 'Gosho', 'asdasd')

SET IDENTITY_INSERT Categories OFF

SET IDENTITY_INSERT Genres ON

INSERT INTO Genres (Id,GenreName, Notes)
VALUES(1, 'Gosho', 'asdasd')
INSERT INTO Genres (Id, GenreName, Notes)
VALUES(2, 'Gosho', 'asdasd')
INSERT INTO Genres (Id, GenreName, Notes)
VALUES(3, 'Gosho', 'asdasd')
INSERT INTO Genres (Id, GenreName, Notes)
VALUES(4, 'Gosho', 'asdasd')
INSERT INTO Genres (Id, GenreName, Notes)
VALUES(5, 'Gosho', 'asdasd')

SET IDENTITY_INSERT Genres OFF

SET IDENTITY_INSERT Directors ON

INSERT INTO Directors (Id, DirectorName, Notes)
VALUES(1, 'Gosho', 'asdasd')
INSERT INTO Directors (Id, DirectorName, Notes)
VALUES(2, 'Gosho', 'asdasd')
INSERT INTO Directors (Id, DirectorName, Notes)
VALUES(3, 'Gosho', 'asdasd')
INSERT INTO Directors (Id, DirectorName, Notes)
VALUES(4, 'Gosho', 'asdasd')
INSERT INTO Directors (Id, DirectorName, Notes)
VALUES(5, 'Gosho', 'asdasd')

-- Problem 14.	Car Rental Database

CREATE DATABASE CarRental 

CREATE TABLE Categories (
Id INT NOT NULL,
CategoryName VARCHAR(50), 
DailyRate INT, 
WeeklyRate INT, 
MonthlyRate INT, 
WeekendRate INT)

ALTER TABLE Categories
ADD CONSTRAINT PK_Categories PRIMARY KEY (Id)

INSERT INTO Categories (Id, CategoryName, DailyRate, WeeklyRate, MonthlyRate, WeekendRate)
VALUES(1, 'Gosho', 1,2,3,4)
INSERT INTO Categories (Id, CategoryName, DailyRate, WeeklyRate, MonthlyRate, WeekendRate)
VALUES(2, 'Gosho', 1,2,3,4)
INSERT INTO Categories (Id, CategoryName, DailyRate, WeeklyRate, MonthlyRate, WeekendRate)
VALUES(3, 'Gosho', 1,2,3,4)

CREATE TABLE Cars (
Id INT NOT NULL,
PlateNumber TINYINT, 
Manufacturer VARCHAR(50), 
Model VARCHAR(50), 
CarYear DATE, 
CategoryId INT,
Doors TINYINT,
Picture VARBINARY(MAX),
Condition VARCHAR(5),
Available BIT)

ALTER TABLE Cars
ADD CONSTRAINT PK_Cars PRIMARY KEY (Id)

INSERT INTO Cars (Id, PlateNumber, Manufacturer, Model, CarYear, CategoryId, Doors, Picture, Condition, Available)
VALUES(1, 32, 'GM','Astra','2002-01-01',4, 4, 21, 'new',0)
INSERT INTO Cars (Id, PlateNumber, Manufacturer, Model, CarYear, CategoryId, Doors, Picture, Condition, Available)
VALUES(2, 32, 'GM','Astra','2002-01-01',4, 4, 21, 'new', 0)
INSERT INTO Cars (Id, PlateNumber, Manufacturer, Model, CarYear, CategoryId, Doors, Picture, Condition, Available)
VALUES(3, 32, 'GM','Astra','2002-01-01',4, 4, 21, 'new', 0)

CREATE TABLE Employees (
Id INT NOT NULL, 
FirstName VARCHAR(20), 
LastName VARCHAR(20), 
Title VARCHAR(100), 
Notes VARCHAR(MAX))

ALTER TABLE Employees
ADD CONSTRAINT PK_Employees PRIMARY KEY (Id)

INSERT INTO Employees (Id, FirstName, LastName, Title, Notes)
VALUES(1,'GM','Astra','title', 'bla')
INSERT INTO Employees (Id, FirstName, LastName, Title, Notes)
VALUES(2,'GM','Astra','title', 'bla')
INSERT INTO Employees (Id, FirstName, LastName, Title, Notes)
VALUES(3,'GM','Astra','title', 'bla')

CREATE TABLE Customers (
Id INT NOT NULL, 
DriverLicenceNumber VARCHAR(10), 
FullName VARCHAR(50), 
Address VARCHAR(50), 
City VARCHAR(50), 
ZIPCode VARCHAR(50), 
Notes VARCHAR(MAX))

ALTER TABLE Customers
ADD CONSTRAINT PK_Customers PRIMARY KEY (Id)

INSERT INTO Customers (Id, DriverLicenceNumber, FullName, Address, City, ZIPCode, Notes)
VALUES(1,'GM','Astra','title', 'bla', 'zip', 'notes')
INSERT INTO Customers (Id, DriverLicenceNumber, FullName, Address, City, ZIPCode, Notes)
VALUES(2,'GM','Astra','title', 'bla', 'zip', 'notes')
INSERT INTO Customers (Id, DriverLicenceNumber, FullName, Address, City, ZIPCode, Notes)
VALUES(3,'GM','Astra','title', 'bla', 'zip', 'notes')

CREATE TABLE RentalOrders (
Id INT NOT NULL, 
EmployeeId INT, 
CustomerId INT, 
CarId INT, 
TankLevel DECIMAL(18,2), 
KilometrageStart TINYINT, 
KilometrageEnd TINYINT, 
TotalKilometrage DECIMAL, 
StartDate DATETIME2, 
EndDate DATETIME2, 
TotalDays INT, 
RateApplied TINYINT, 
TaxRate DECIMAL, 
OrderStatus BIT, 
Notes VARCHAR(MAX))

ALTER TABLE RentalOrders
ADD CONSTRAINT PK_RentalOrders PRIMARY KEY (Id)

INSERT INTO RentalOrders (Id, EmployeeId, CustomerId, CarId, TankLevel, KilometrageStart, 
KilometrageEnd, TotalKilometrage, StartDate, EndDate, TotalDays, RateApplied, TaxRate, 
OrderStatus, Notes)
VALUES(1,1,1,1, 2, 2, 2, 2, '2000-01-01', '2000-02-02', 1, 2, 3.2, 1, 'bla')
INSERT INTO RentalOrders (Id, EmployeeId, CustomerId, CarId, TankLevel, KilometrageStart, 
KilometrageEnd, TotalKilometrage, StartDate, EndDate, TotalDays, RateApplied, TaxRate, 
OrderStatus, Notes)
VALUES(2,1,1,1, 2, 2, 2, 2, '2000-01-01', '2000-02-02', 1, 2, 3.2, 1, 'bla')
INSERT INTO RentalOrders (Id, EmployeeId, CustomerId, CarId, TankLevel, KilometrageStart, 
KilometrageEnd, TotalKilometrage, StartDate, EndDate, TotalDays, RateApplied, TaxRate, 
OrderStatus, Notes)
VALUES(3,1,1,1, 2, 2, 2, 2, '2000-01-01', '2000-02-02', 1, 2, 3.2, 1, 'bla')

-- Problem 15.	Hotel Database

CREATE DATABASE Hotel

CREATE TABLE Employees (
Id INT NOT NULL, 
FirstName VARCHAR(20), 
LastName VARCHAR(20), 
Title VARCHAR(50), 
Notes VARCHAR(MAX))

ALTER TABLE Employees
ADD CONSTRAINT PK_Employees PRIMARY KEY (Id)

INSERT INTO Employees (Id, FirstName, LastName, Notes)
VALUES(1,'GM','Astra','notes')
INSERT INTO Employees (Id, FirstName, LastName, Notes)
VALUES(2,'GM','Astra','notes')
INSERT INTO Employees (Id, FirstName, LastName, Notes)
VALUES(3,'GM','Astra','notes')

CREATE TABLE Customers (
AccountNumber VARCHAR(10) NOT NULL, 
FirstName VARCHAR(20), 
LastName VARCHAR(20), 
PhoneNumber VARCHAR(20), 
EmergencyName VARCHAR(20), 
EmergencyNumber VARCHAR(20), 
Notes VARCHAR(MAX))

ALTER TABLE Customers
ADD CONSTRAINT PK_Customers PRIMARY KEY (AccountNumber)

INSERT INTO Customers (AccountNumber, FirstName, LastName, PhoneNumber, EmergencyName, EmergencyNumber, Notes)
VALUES('a','GM','Astra','notes', 'emg', 'emgn', 'bla')
INSERT INTO Customers (AccountNumber, FirstName, LastName, PhoneNumber, EmergencyName, EmergencyNumber, Notes)
VALUES('b','GM','Astra','notes', 'emg', 'emgn', 'bla')
INSERT INTO Customers (AccountNumber, FirstName, LastName, PhoneNumber, EmergencyName, EmergencyNumber, Notes)
VALUES('c','GM','Astra','notes', 'emg', 'emgn', 'bla')

CREATE TABLE RoomStatus (
RoomStatus VARCHAR(5) NOT NULL, 
Notes VARCHAR(MAX))

ALTER TABLE RoomStatus
ADD CONSTRAINT PK_RoomStatus PRIMARY KEY (RoomStatus)

INSERT INTO RoomStatus (RoomStatus, Notes)
VALUES('a','GM')
INSERT INTO RoomStatus (RoomStatus, Notes)
VALUES('b','GM')
INSERT INTO RoomStatus (RoomStatus, Notes)
VALUES('c','GM')

CREATE TABLE RoomTypes (
RoomType VARCHAR(5) NOT NULL, 
Notes  VARCHAR(MAX))

ALTER TABLE RoomTypes
ADD CONSTRAINT PK_RoomTypes PRIMARY KEY (RoomType)

INSERT INTO RoomTypes (RoomType, Notes)
VALUES('a','GM')
INSERT INTO RoomTypes (RoomType, Notes)
VALUES('b','GM')
INSERT INTO RoomTypes (RoomType, Notes)
VALUES('c','GM')

CREATE TABLE BedTypes (
BedType VARCHAR(5) NOT NULL, 
Notes VARCHAR(MAX))

ALTER TABLE BedTypes
ADD CONSTRAINT PK_BedTypes PRIMARY KEY (BedType)

INSERT INTO BedTypes (BedType, Notes)
VALUES('a','GM')
INSERT INTO BedTypes (BedType, Notes)
VALUES('b','GM')
INSERT INTO BedTypes (BedType, Notes)
VALUES('c','GM')

CREATE TABLE Rooms (
RoomNumber INT NOT NULL, 
RoomType VARCHAR(10), 
BedType  VARCHAR(10), 
Rate TINYINT, 
RoomStatus BIT, 
Notes VARCHAR(MAX))

ALTER TABLE Rooms
ADD CONSTRAINT PK_Rooms PRIMARY KEY (RoomNumber)

INSERT INTO Rooms (RoomNumber, RoomType, BedType, Rate, RoomStatus, Notes)
VALUES(1,'GM', 'type', 1, 0, 'bla')
INSERT INTO Rooms (RoomNumber, RoomType, BedType, Rate, RoomStatus, Notes)
VALUES(2,'GM', 'type', 1, 0, 'bla')
INSERT INTO Rooms (RoomNumber, RoomType, BedType, Rate, RoomStatus, Notes)
VALUES(3,'GM', 'type', 1, 0, 'bla')

CREATE TABLE Payments (
Id INT NOT NULL, 
EmployeeId INT, 
PaymentDate DATETIME2, 
AccountNumber INT, 
FirstDateOccupied DATETIME2, 
LastDateOccupied DATETIME2, 
TotalDays INT, 
AmountCharged DECIMAL(18,2), 
TaxRate DECIMAL(18,2), 
TaxAmount DECIMAL(18,2), 
PaymentTotal DECIMAL(18,2), 
Notes VARCHAR(MAX))

ALTER TABLE Payments
ADD CONSTRAINT PK_Payments PRIMARY KEY (Id)

INSERT INTO Payments (Id, EmployeeId, PaymentDate, AccountNumber, FirstDateOccupied, LastDateOccupied,
TotalDays, AmountCharged, TaxRate, TaxAmount, PaymentTotal, Notes)
VALUES(1,1, '2000-01-01', 1, '2000-01-01', '2000-01-01', 1, 1.1, 1.1, 1.1, 1.1, 'bla')
INSERT INTO Payments (Id, EmployeeId, PaymentDate, AccountNumber, FirstDateOccupied, LastDateOccupied,
TotalDays, AmountCharged, TaxRate, TaxAmount, PaymentTotal, Notes)
VALUES(2,1, '2000-01-01', 1, '2000-01-01', '2000-01-01', 1, 1.1, 1.1, 1.1, 1.1, 'bla')
INSERT INTO Payments (Id, EmployeeId, PaymentDate, AccountNumber, FirstDateOccupied, LastDateOccupied,
TotalDays, AmountCharged, TaxRate, TaxAmount, PaymentTotal, Notes)
VALUES(3,1, '2000-01-01', 1, '2000-01-01', '2000-01-01', 1, 1.1, 1.1, 1.1, 1.1, 'bla')

CREATE TABLE Occupancies (
Id INT NOT NULL, 
EmployeeId INT, 
DateOccupied DATETIME2, 
AccountNumber INT, 
RoomNumber INT, 
RateApplied DECIMAL, 
PhoneCharge DECIMAL, 
Notes VARCHAR(MAX))

ALTER TABLE Occupancies
ADD CONSTRAINT PK_Occupancies PRIMARY KEY (Id)

INSERT INTO Occupancies (Id, EmployeeId, DateOccupied, AccountNumber, RoomNumber, RateApplied,
PhoneCharge, Notes)
VALUES(1,1, '2000-01-01', 1, 1, 1.1, 1.1, 'bla')
INSERT INTO Occupancies (Id, EmployeeId, DateOccupied, AccountNumber, RoomNumber, RateApplied,
PhoneCharge, Notes)
VALUES(2,1, '2000-01-01', 1, 1, 1.1, 1.1, 'bla')
INSERT INTO Occupancies (Id, EmployeeId, DateOccupied, AccountNumber, RoomNumber, RateApplied,
PhoneCharge, Notes)
VALUES(3,1, '2000-01-01', 1, 1, 1.1, 1.1, 'bla')

-- Problem 16.	Create SoftUni Database

CREATE DATABASE SoftUni

CREATE TABLE Towns (
Id INT IDENTITY(1,1) NOT NULL, 
Name VARCHAR(50))

ALTER TABLE Towns
ADD CONSTRAINT PK_Towns PRIMARY KEY (Id)

CREATE TABLE Addresses (
Id INT IDENTITY(1,1) NOT NULL, 
AddressText VARCHAR(50), 
TownId INT)

ALTER TABLE Addresses
ADD CONSTRAINT FK_AddressesToTown FOREIGN KEY (TownId) REFERENCES Towns (Id)

ALTER TABLE Addresses
ADD CONSTRAINT PK_Addresses PRIMARY KEY (Id)

CREATE TABLE Departments (
Id INT IDENTITY(1,1) NOT NULL, 
Name VARCHAR(50))

ALTER TABLE Departments
ADD CONSTRAINT PK_Departments PRIMARY KEY (Id)

CREATE TABLE Employees (
Id INT IDENTITY(1,1) NOT NULL, 
FirstName VARCHAR(50), 
MiddleName VARCHAR(50), 
LastName VARCHAR(50), 
JobTitle VARCHAR(50), 
DepartmentId INT, 
HireDate DATETIME2, 
Salary DECIMAL(18, 2), 
AddressId INT)

DROP TABLE Employees

ALTER TABLE Employees
ADD CONSTRAINT FK_EmployeesToDepartment FOREIGN KEY (DepartmentId) REFERENCES Departments (Id)

ALTER TABLE Employees
ADD CONSTRAINT FK_EmployeesToAddress FOREIGN KEY (AddressId) REFERENCES Addresses (Id)

ALTER TABLE Employees
ADD CONSTRAINT PK_Employees PRIMARY KEY (Id)

-- Problem 17.	Backup Database

-- Problem 18.	Basic Insert

INSERT INTO Towns(Name)
VALUES('Sofia')
INSERT INTO Towns(Name)
VALUES('Plovdiv')
INSERT INTO Towns(Name)
VALUES('Varna')
INSERT INTO Towns(Name)
VALUES('Burgas')

INSERT INTO Departments (Name)
VALUES('Engineering')
INSERT INTO Departments (Name)
VALUES('Sales')
INSERT INTO Departments (Name)
VALUES('Marketing')
INSERT INTO Departments (Name)
VALUES('Software Development')
INSERT INTO Departments (Name)
VALUES('Quality Assurance')

INSERT INTO Employees (Salary)
VALUES(2.3)
INSERT INTO Employees (Salary)
VALUES(2.4)
INSERT INTO Employees (Salary)
VALUES(2.5)

-- TODO

-- Problem 19.	Basic Select All Fields

SELECT * FROM Towns
SELECT * FROM Departments
SELECT * FROM Employees

-- Problem 20.	Basic Select All Fields and Order Them

SELECT * FROM Towns
ORDER BY Name
SELECT * FROM Departments
ORDER BY Name
SELECT * FROM Employees
ORDER BY Salary DESC

-- Problem 21.	Basic Select Some Fields

SELECT Name FROM Towns
ORDER BY Name
SELECT Name FROM Departments
ORDER BY Name
SELECT FirstName, LastName, JobTitle, Salary FROM Employees
ORDER BY Salary DESC

-- Problem 22.	Increase Employees Salary

UPDATE Employees
SET Salary = Salary + Salary * 0.1
SELECT Salary FROM Employees 

-- Problem 23.	Decrease Tax Rate

UPDATE Payments
SET TaxRate = TaxRate - TaxRate * 0.03
SELECT TaxRate FROM Payments

-- Problem 24.	Delete All Records

TRUNCATE TABLE Occupancies 