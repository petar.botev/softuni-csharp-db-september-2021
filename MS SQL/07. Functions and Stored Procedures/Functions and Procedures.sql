-- 01. Employees with Salary Above 35000

CREATE PROC usp_GetEmployeesSalaryAbove35000 
AS
SELECT FirstName AS [First Name], LastName AS [Last Name]
FROM Employees
WHERE Salary > 35000

EXEC dbo.usp_GetEmployeesSalaryAbove35000

-- 02. Employees with Salary Above Number

CREATE PROC usp_GetEmployeesSalaryAboveNumber (@num DECIMAL(18,4))
AS
SELECT FirstName AS [First Name], LastName AS [Last Name]
FROM Employees
WHERE Salary >= @num

EXEC usp_GetEmployeesSalaryAboveNumber 48100

-- 03. Town Names Starting With

CREATE PROC usp_GetTownsStartingWith (@str VARCHAR(50))
AS
SELECT Name AS [Town]
FROM Towns
WHERE LEFT(Name, LEN(@str)) = @str

EXEC usp_GetTownsStartingWith 'Be'

-- 04. Employees from Town

CREATE PROC usp_GetEmployeesFromTown (@townName VARCHAR(50))
AS
SELECT FirstName AS [First Name], LastName AS [Last Name] 
FROM Employees e
JOIN Addresses a ON e.AddressID = a.AddressID
JOIN Towns t ON a.TownID = t.TownID
WHERE t.Name = @townName

EXEC usp_GetEmployeesFromTown Sofia

-- 05. Salary Level Function

CREATE FUNCTION ufn_GetSalaryLevel(@salary DECIMAL(18,4))
RETURNS VARCHAR(7)
AS
BEGIN
DECLARE @result VARCHAR(7)
	IF(@salary < 30000)
		SET @result = 'Low'
	ELSE IF (@salary BETWEEN 30000 AND 50000)
		SET @result = 'Average'
	ELSE
		SET @result = 'High'
	RETURN @result
END

SELECT Salary, dbo.ufn_GetSalaryLevel(Salary)
FROM Employees

-- 06. Employees by Salary Level

CREATE PROC usp_EmployeesBySalaryLevel (@level VARCHAR(7))
AS
SELECT FirstName AS [First Name], LastName AS [Last Name]
FROM Employees
WHERE @level = dbo.ufn_GetSalaryLevel(Salary)

EXEC usp_EmployeesBySalaryLevel 'High'

-- 07. Define Function

CREATE FUNCTION ufn_IsWordComprised(@setOfLetters VARCHAR(50), @word VARCHAR(50)) 
RETURNS BIT
AS
BEGIN
DECLARE @hasLetter BIT = 0
DECLARE @i INT = 1
DECLARE @j INT = 1

 WHILE LEN(@word) >= @i
 BEGIN
	SET @j = 1
	WHILE LEN(@setOfLetters) >= @j
	BEGIN

	  IF(SUBSTRING(@word,@i,1) = SUBSTRING(@setOfLetters, @j, 1))
		SET @hasLetter = 1
	  		
	  SET	@j = @j + 1
	END
	IF(@hasLetter = 0)
		RETURN 0
	ELSE 
		SET @hasLetter = 0
   SET	@i = @i + 1
  END
 RETURN 1
END

SELECT Name, dbo.ufn_IsWordComprised('oistmiahf', Name)
FROM Towns

-- 08. Delete Employees and Departments

CREATE PROC usp_DeleteEmployeesFromDepartment (@departmentId INT)
AS

DELETE 
FROM EmployeesProjects
WHERE EmployeeID IN(SELECT EmployeeID FROM Employees WHERE DepartmentID = @departmentId)

UPDATE Employees
SET ManagerID = NULL
WHERE ManagerID IN(SELECT EmployeeID FROM Employees WHERE DepartmentID = @departmentId)

ALTER TABLE Departments
ALTER COLUMN ManagerID INT NULL

UPDATE Departments
SET ManagerID = NULL
WHERE ManagerID IN(SELECT EmployeeID FROM Employees WHERE DepartmentID = @departmentId)

DELETE
FROM Employees
WHERE DepartmentID = @departmentId

DELETE 
FROM Departments
WHERE DepartmentID = @departmentId

SELECT COUNT(*)
FROM Employees
WHERE DepartmentID = @departmentId

EXEC dbo.usp_DeleteEmployeesFromDepartment 4

-- 09. Find Full Name

CREATE PROC usp_GetHoldersFullName 
AS
SELECT CONCAT(FirstName, ' ', LastName) AS [Full Name]
FROM AccountHolders

-- 10. People with Balance Higher Than

CREATE PROC usp_GetHoldersWithBalanceHigherThan (@number DECIMAL(18,2))
AS
SELECT acch.FirstName AS [First Name], acch.LastName AS [Last Name]
FROM AccountHolders acch
JOIN Accounts a ON acch.Id = a.AccountHolderId
GROUP BY acch.FirstName, acch.LastName
HAVING SUM(a.Balance) > @number
ORDER BY FirstName, LastName

EXEC dbo.usp_GetHoldersWithBalanceHigherThan 10000

-- 11. Future Value Function

CREATE FUNCTION ufn_CalculateFutureValue (@sum DECIMAL(18,2), @yearlyInterest FLOAT, @years INT)
RETURNS DECIMAL(18,4)
BEGIN
	DECLARE @result DECIMAL(18, 4)
	SET @result = (@sum * POWER((1 + @yearlyInterest), @years))
	RETURN @result
END

SELECT dbo.ufn_CalculateFutureValue(1000, 0.1, 5) AS Output

-- 12. Calculating Interest

CREATE PROC usp_CalculateFutureValueForAccount(@accountId INT, @interestRate FLOAT)
AS
SELECT a.Id AS [Account Id]
     , ah.FirstName AS [First Name]
	 , ah.LastName AS [Last Name]
	 , a.Balance AS [Current Balance]
     , dbo.ufn_CalculateFutureValue(a.Balance, @interestRate, 5) AS [Balance in 5 years]
FROM AccountHolders ah
JOIN Accounts a ON ah.Id = a.AccountHolderId
WHERE a.Id = @accountId

EXEC dbo.usp_CalculateFutureValueForAccount 1, 0.1

-- 13. *Cash in User Games Odd Rows


