-- 01. Find Names of All Employees by First Name

SELECT FirstName, LastName
FROM Employees
WHERE LEFT(FirstName, 2) = 'Sa'

-- 02. Find Names of All Employees by Last Name

SELECT FirstName, LastName
FROM Employees
WHERE LastName LIKE '%ei%'

-- 03. Find First Names of All Employess

SELECT FirstName
FROM Employees
WHERE DepartmentID IN (3, 10) AND FORMAT(HireDate, 'yyyy') BETWEEN 1995 AND 2005

-- 04. Find All Employees Except Engineers

SELECT FirstName, LastName
FROM Employees
WHERE JobTitle NOT LIKE '%engineer%'

-- 05. Find Towns with Name Length

SELECT Name
FROM Towns
WHERE LEN(Name) = 5 OR LEN(Name) = 6
ORDER BY Name

-- 06. Find Towns Starting With

SELECT TownID, Name
FROM Towns
WHERE LEFT(Name,1) LIKE '[MKBE]'
ORDER BY Name 

-- 07. Find Towns Not Starting With

SELECT TownID, Name
FROM Towns
WHERE LEFT(Name,1) LIKE '[^RBD]'
ORDER BY Name 

-- 08. Create View Employees Hired After
CREATE VIEW V_EmployeesHiredAfter2000 AS
SELECT FirstName, LastName
FROM Employees
WHERE YEAR(HireDate) > 2000

SELECT *
FROM V_EmployeesHiredAfter2000

-- 09. Length of Last Name

SELECT FirstName, LastName
FROM Employees
WHERE LEN(LastName) = 5

-- 10. Rank Employees by Salary

SELECT EmployeeID, FirstName, LastName, Salary, DENSE_RANK()
										OVER(
										PARTITION BY Salary
										ORDER BY EmployeeID
										) AS Rank

FROM Employees
WHERE Salary BETWEEN 10000 AND 50000
ORDER BY Salary DESC

-- 11. Find All Employees with Rank 2 (not included in final score)
SELECT *
FROM(
SELECT EmployeeID, FirstName, LastName, Salary, DENSE_RANK()
										OVER(
										PARTITION BY Salary
										ORDER BY EmployeeID
										)[Rank]
										

FROM Employees
WHERE Salary BETWEEN 10000 AND 50000 
) AS T
WHERE [Rank] = 2
ORDER BY Salary DESC

-- 12. Countries Holding 'A'

SELECT CountryName AS [Country Name], IsoCode AS [ISO Code]
FROM Countries
WHERE LEN(CountryName) - LEN(REPLACE(CountryName, 'A', '')) >= 3
ORDER BY IsoCode

SELECT *
FROM Countries

-- 13. Mix of Peak and River Names

SELECT PeakName, RiverName, LOWER(PeakName + SUBSTRING(RiverName, 2, LEN(RiverName))) AS Mix
FROM Peaks, Rivers
WHERE RIGHT(PeakName,1) = LEFT(RiverName,1)
ORDER BY Mix

-- 14. Games From 2011 and 2012 Year

SELECT TOP(50) Name, FORMAT(Start, 'yyyy-MM-dd') AS Start
FROM Games
WHERE YEAR(Start) IN (2011 , 2012)
ORDER BY Start, Name

-- 15. User Email Providers

SELECT Username, SUBSTRING(Email, CHARINDEX('@', Email) + 1, LEN(Email) - CHARINDEX('@', Email)) AS [Email Provider]
FROM Users
ORDER BY [Email Provider], Username

-- 16. Get Users with IPAddress Like Pattern

SELECT Username, IpAddress [IP Address]
FROM Users
WHERE IpAddress LIKE '___.1%.%.___' 
ORDER BY Username

-- 17. Show All Games with Duration

SELECT Name,
CASE
  WHEN  DATEPART(HOUR, CAST(Start AS time)) >= 0 AND  DATEPART(HOUR, CAST(Start AS time)) < 12 THEN 'Morning'
  WHEN  DATEPART(HOUR, CAST(Start AS time)) >= 12 AND  DATEPART(HOUR, CAST(Start AS time)) < 18 THEN 'Afternoon '
  WHEN  DATEPART(HOUR, CAST(Start AS time)) >= 18 AND  DATEPART(HOUR, CAST(Start AS time)) < 24 THEN 'Evening '
  ELSE ''
END 
   AS [Part of the Day], 
CASE
  WHEN Duration <= 3 THEN 'Extra Short'
  WHEN Duration BETWEEN 4 AND 6 THEN 'Short'
  WHEN Duration > 6 THEN 'Long'
  WHEN Duration IS NULL THEN 'Extra Long' 	
  ELSE ''
END		
   AS Duration											
FROM Games
ORDER BY Name, Duration

-- 18. Orders Table

CREATE TABLE Orders(
Id INT IDENTITY PRIMARY KEY NOT NULL,
ProductName NVARCHAR(20),
OrderDate DATETIME2)

INSERT INTO Orders(ProductName, OrderDate)
VALUES('Butter', '2016-09-19 00:00:00.000'),
	  ('Milk', '2016-09-30 00:00:00.000'),
	  ('Cheese', '2016-09-04 00:00:00.000'),
	  ('Bread', '2015-12-20 00:00:00.000'),
	  ('Tomatoes', '2015-12-30 00:00:00.000')

SELECT ProductName, OrderDate, DATEADD(DAY, 3, OrderDate) AS [Pay Due], DATEADD(MONTH, 1, OrderDate) AS [Deliver Due]
FROM Orders

-- Problem 19.	 People Table

CREATE TABLE People(
Id INT IDENTITY PRIMARY KEY NOT NULL,
Name NVARCHAR(20),
Birthdate DATETIME2)

INSERT INTO People(Name, Birthdate)
VALUES('Victor', '2000-12-07 00:00:00.000'),
	  ('Steven', '1992-09-10 00:00:00.000'),
	  ('Stephen', '1910-09-19 00:00:00.000'),
	  ('John', '2010-01-06 00:00:00.000'),
	  ('Tomatoes', '2015-12-30 00:00:00.000')

SELECT Name, DATEDIFF(YEAR, Birthdate, GETDATE()) AS [Age in Years]
           , DATEDIFF(MONTH, Birthdate, GETDATE()) AS [Age in Months]
		   , DATEDIFF(DAY, Birthdate, GETDATE()) AS [Age in Days]
		   , DATEDIFF(MINUTE, Birthdate, GETDATE()) AS [Age in Minutes]
FROM People