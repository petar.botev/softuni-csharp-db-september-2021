CREATE DATABASE [Table Relations]

-- 01. One-To-One Relationship

CREATE TABLE Persons(
Id INT IDENTITY,
FirstName VARCHAR(20),
Salary DECIMAL(18,2),
PassportID INT)

INSERT INTO Persons(FirstName, Salary, PassportID)
VALUES ('Roberto', 43300.00, 102),
       ('Tom', 56100.00, 103),
	   ('Yana', 60200.00, 101)

ALTER TABLE Persons
ADD CONSTRAINT PK_Person PRIMARY KEY (Id);

CREATE TABLE Passports(
Id INT, 
PassportNumber VARCHAR(20))

INSERT INTO Passports(Id, PassportNumber)
VALUES (101, 'N34FG21B'),
       (102, 'K65LO4R7'),
	   (103, 'ZE657QP2')

ALTER TABLE Passports
ALTER COLUMN Id INT NOT NULL;

ALTER TABLE Passports
ADD CONSTRAINT PK_Passports PRIMARY KEY (Id);

ALTER TABLE Persons
ADD FOREIGN KEY (PassportID) REFERENCES Passports(Id);

-- 02. One-To-Many Relationship

DROP TABLE Models
DROP TABLE Manufacturers

CREATE TABLE Manufacturers (
Id INT IDENTITY(1,1),
Name VARCHAR(50),
EstablishedOn VARCHAR(15))

ALTER TABLE Manufacturers
ADD CONSTRAINT PK_Manufacturers PRIMARY KEY(Id)

INSERT INTO Manufacturers(Name, EstablishedOn)
VALUES ('BMW', '07/03/1916'),
	   ('Tesla', '01/01/2003'),
	   ('Lada', '01/05/1966')

CREATE TABLE Models (
Id INT IDENTITY(101,1),
Name VARCHAR(50),
ManufacturerID INT REFERENCES Manufacturers(Id))

INSERT INTO Models(Name, ManufacturerID)
VALUES ('X1', 1),
	   ('i6', 1),
	   ('Model S', 2),
	   ('Model X', 2),
	   ('Model 3', 2),
	   ('Nova', 3)

ALTER TABLE Models
ADD CONSTRAINT PK_Models PRIMARY KEY(Id)

--ALTER TABLE Models
--ADD CONSTRAINT FK_ModelsManufacturers
--FOREIGN KEY (ManufacturerID) REFERENCES Manufacturers(Id);

-- 03. Many-To-Many Relationship
DROP TABLE StudentsExams
DROP TABLE Students
DROP TABLE Exams


CREATE TABLE Students(
Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
Name VARCHAR(50))

INSERT INTO Students(Name)
VALUES ('Mila'),
	  ('Toni'),
	  ('Ron')

CREATE TABLE Exams(
Id INT IDENTITY(101,1) PRIMARY KEY NOT NULL,
Name VARCHAR(50))

INSERT INTO Exams(Name)
VALUES ('SpringMVC'),
	  ('Neo4j'),
	  ('Oracle 11g')

CREATE TABLE StudentsExams(
StudentID INT NOT NULL,
ExamID INT NOT NULL)

INSERT INTO StudentsExams(StudentID, ExamID)
VALUES (1, 101),
	  (1, 102),
	  (2, 101),
	  (3, 103),
	  (2, 102),
	  (2, 103)

ALTER TABLE StudentsExams
ADD PRIMARY KEY(StudentID, ExamID)

ALTER TABLE StudentsExams
ADD FOREIGN KEY (StudentID) REFERENCES Students(Id)

ALTER TABLE StudentsExams
ADD FOREIGN KEY (ExamID) REFERENCES Exams(Id)

-- 04. Self-Referencing

CREATE TABLE Teachers(
Id INT IDENTITY(101, 1) PRIMARY KEY NOT NULL,
Name VARCHAR(50),
ManagerID INT 
)

ALTER TABLE Teachers
ADD FOREIGN KEY (ManagerID) REFERENCES Teachers(Id)

INSERT INTO Teachers
VALUES('John', NULL),
	  ('Maya', 106),
	  ('Silvia', 106),
	  ('Ted', 105),
	  ('Mark', 101),
	  ('Greta', 101)

-- 05. Online Store Database

CREATE DATABASE [Online store]

CREATE TABLE Orders(
Id INT IDENTITY(1,1) PRIMARY KEY,
CustomerID INT
)

CREATE TABLE Customers(
Id INT IDENTITY(1,1) PRIMARY KEY,
Name VARCHAR(50),
Birthday DATETIME2,
CityID INT
)

CREATE TABLE Cities(
Id INT IDENTITY(1,1) PRIMARY KEY,
Name VARCHAR(50)
)

CREATE TABLE Items(
Id INT IDENTITY(1,1) PRIMARY KEY,
Name VARCHAR(50),
ItemTypeID INT
)

CREATE TABLE ItemTypes(
Id INT IDENTITY(1,1) PRIMARY KEY,
Name VARCHAR(50)
)

CREATE TABLE OrderItems(
OrderID INT NOT NULL,
ItemID INT NOT NULL
)

ALTER TABLE OrderItems
ADD PRIMARY KEY (OrderID, ItemID)

ALTER TABLE Orders
ADD FOREIGN KEY (CustomerID) REFERENCES Customers (Id)

ALTER TABLE Customers
ADD FOREIGN KEY (CityID) REFERENCES Cities(Id)

ALTER TABLE Items
ADD FOREIGN KEY (ItemTypeID) REFERENCES ItemTypes(Id)

ALTER TABLE OrderItems
ADD FOREIGN KEY (OrderID) REFERENCES Orders(Id)

ALTER TABLE OrderItems
ADD FOREIGN KEY (ItemID) REFERENCES Items(Id)

-- 06. University Database

CREATE DATABASE University

CREATE TABLE Majors(
Id INT IDENTITY(1,1) PRIMARY KEY,
Name VARCHAR(50)
)

CREATE TABLE Students(
Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
StudentNumber VARCHAR(50),
StudentName VARCHAR(50),
MajorID INT
)

CREATE TABLE Payments(
Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
PaymentDate DATETIME2,
PaymentAmount DECIMAL(18,2),
StudentID INT
)

CREATE TABLE Agenda(
StudentID INT NOT NULL,
SubjectID INT NOT NULL
)

ALTER TABLE Agenda
ADD PRIMARY KEY (StudentID, SubjectID)

CREATE TABLE Subjects(
Id INT IDENTITY(1,1) PRIMARY KEY,
SubjectName VARCHAR(50)
)

ALTER TABLE Students
ADD FOREIGN KEY (MajorID) REFERENCES Majors(Id)

ALTER TABLE Payments
ADD FOREIGN KEY (StudentID) REFERENCES Students(Id)

ALTER TABLE Agenda
ADD FOREIGN KEY (StudentID) REFERENCES Students(Id)

ALTER TABLE Agenda
ADD FOREIGN KEY (SubjectID) REFERENCES Subjects(Id)

-- 09. *Peaks in Rila

SELECT m.MountainRange, p.PeakName, p.Elevation
FROM Peaks AS p
JOIN Mountains AS m ON p.MountainId = m.Id
WHERE m.MountainRange = 'Rila'
ORDER BY p.Elevation DESC