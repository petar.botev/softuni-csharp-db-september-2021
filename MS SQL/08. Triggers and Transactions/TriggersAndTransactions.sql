CREATE TABLE Logs (
	  LogId INT PRIMARY KEY IDENTITY
	, AccountId INT REFERENCES Accounts(Id)
	, OldSum DECIMAL(18,2)
	, NewSum DECIMAL(18,2)
	)

GO

CREATE TRIGGER tr_InsertAccountInfo 
ON Accounts FOR UPDATE
AS
DECLARE @oldSum DECIMAL(18,2) = (SELECT Balance FROM deleted)
DECLARE @newSum DECIMAL(18,2) = (SELECT Balance FROM inserted)
DECLARE @sumId DECIMAL(18,2) = (SELECT Id FROM inserted)

INSERT INTO Logs (AccountId, OldSum, NewSum)
VALUES (@sumId, @oldSum, @newSum)

UPDATE Accounts
SET Balance += 10
WHERE Id = 1

SELECT *
FROM Logs