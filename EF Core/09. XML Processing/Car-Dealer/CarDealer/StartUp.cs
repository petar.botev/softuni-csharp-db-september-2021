﻿using CarDealer.Data;
using System.IO;
using System;
using System.Xml.Serialization;
using CarDealer.Models;
using CarDealer.Dtos.Import;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;
using CarDealer.Dtos.Export;
using System.Text;

namespace CarDealer
{
    public class StartUp
    {
        private static IMapper mapper;
        public static void Main(string[] args)
        {
            var context = new CarDealerContext();
            //context.Database.EnsureDeleted();
            //context.Database.EnsureCreated();
            //string input = File.ReadAllText(@"..\..\..\Datasets\suppliers.xml");
            //string input2 = File.ReadAllText(@"..\..\..\Datasets\parts.xml");
            //string input3 = File.ReadAllText(@"..\..\..\Datasets\cars.xml");
            //string input4 = File.ReadAllText(@"..\..\..\Datasets\customers.xml");
            //string input5 = File.ReadAllText(@"..\..\..\Datasets\sales.xml");

            //Console.WriteLine(ImportSuppliers(context, input));
            //Console.WriteLine(ImportParts(context, input2));
            //Console.WriteLine(ImportCars(context, input3));
            //Console.WriteLine(ImportCustomers(context, input4));
            //Console.WriteLine(ImportSales(context, input5));

            Console.WriteLine(GetSalesWithAppliedDiscount(context));
        }

        //19. Export Sales With Applied Discount
        public static string GetSalesWithAppliedDiscount(CarDealerContext context)
        {
            var salesDto = context.Sales
                .Select(s => new ExportSalesDto
                {
                    Car = new ExportSoldCarDto
                    {
                        Make = s.Car.Make,
                        Model = s.Car.Model,
                        TravelledDistance = s.Car.TravelledDistance.ToString()
                    },
                    Discount = s.Discount.ToString("f0"),
                    CustomerName = s.Customer.Name,
                    Price = s.Car.PartCars.Sum(cp => cp.Part.Price).ToString(),
                    PriceWithDiscount = (s.Car.PartCars.Sum(cp => cp.Part.Price) - s.Car.PartCars.Sum(cp => cp.Part.Price) *  s.Discount / 100).ToString()
                })
                .ToArray();

            var serializer = new XmlSerializer(typeof(ExportSalesDto[]), new XmlRootAttribute("sales"));

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            using StringWriter strr = new StringWriter();

            serializer.Serialize(strr, salesDto, ns);

            return strr.ToString();
        }

        // 18. Export Total Sales By Customer
        public static string GetTotalSalesByCustomer(CarDealerContext context)
        {
            var customersDto = context.Customers
                .Where(c => c.Sales.Count > 0)
                .Select(c => new ExportCustomersDto
                {
                    Name = c.Name,
                    Cars = c.Sales.Count,
                    TotalSpentMoney = c.Sales.SelectMany(x => x.Car.PartCars).Sum(cp => cp.Part.Price)
                })
                .OrderByDescending(c => c.TotalSpentMoney)
                .ToArray();

            var serializer = new XmlSerializer(typeof(ExportCustomersDto[]), new XmlRootAttribute("customers"));

            var strr = new StringWriter();

            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            serializer.Serialize(strr, customersDto, ns);

            return strr.ToString();
        }

        //17. Export Cars With Their List Of Parts
        public static string GetCarsWithTheirListOfParts(CarDealerContext context)
        {
            var carsDto = context.Cars
                .Select(c => new ExportCarsWithPartsDto
                {
                    Make = c.Make,
                    Model = c.Model,
                    TravelledDistance = c.TravelledDistance,
                    Parts = c.PartCars.Select(pc => new ExportCarsPartsDto
                    {
                        Name = pc.Part.Name,
                        Price = pc.Part.Price
                    }).OrderByDescending(p => p.Price).ToList()
                })
                .OrderByDescending(c => c.TravelledDistance)
                .ThenBy(c => c.Model)
                .Take(5)
                .ToArray();

            var serializer = new XmlSerializer(typeof(ExportCarsWithPartsDto[]), new XmlRootAttribute("cars"));

            using var strr = new StringWriter();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            serializer.Serialize(strr, carsDto, ns);

            return strr.ToString();
        }

        //16. Export Local Suppliers
        public static string GetLocalSuppliers(CarDealerContext context)
        {
            var carsDto = context.Suppliers
                .Where(s => s.IsImporter == false)
                .Select(s => new ExportSuppliersDto
                {
                    Id = s.Id,
                    Name = s.Name,
                    Parts = s.Parts.Count
                })
                .ToArray();

            using StringWriter strr = new StringWriter();
            var serializer = new XmlSerializer(typeof(ExportSuppliersDto[]), new XmlRootAttribute("suppliers"));

            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            serializer.Serialize(strr, carsDto, ns);

            return strr.ToString();
        }

        //15. Export Cars From Make BMW
        public static string GetCarsFromMakeBmw(CarDealerContext context)
        {
            var carsDto = context.Cars
                .Where(c => c.Make == "BMW")
                .Select(c => new ExportBMWDto
                {
                    Id = c.Id,
                    Model = c.Model,
                    TravelledDistance = c.TravelledDistance
                })
                .OrderBy(c => c.Model)
                .ThenByDescending(c => c.TravelledDistance)
                .ToArray();

            using StringWriter strr = new StringWriter();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            var serializer = new XmlSerializer(typeof(ExportBMWDto[]), new XmlRootAttribute("cars"));
            serializer.Serialize(strr, carsDto, ns);

            return strr.ToString();
        }

        //14. Export Cars With Distance
        public static string GetCarsWithDistance(CarDealerContext context)
        {
            var cars = context.Cars
                .Select(c => new ExportCarsWithDistanceDto
                {
                    Make = c.Make,
                    Model = c.Model,
                    TravelledDistance = c.TravelledDistance
                })
                .OrderBy(c => c.Make)
                .ThenBy(c => c.Model)
                .Take(10)
                .ToArray();

            XmlSerializer serializer = new XmlSerializer(typeof(ExportCarsWithDistanceDto[]), new XmlRootAttribute("cars"));

            
            using StringWriter strr = new StringWriter();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            serializer.Serialize(strr, cars, ns);

            return strr.ToString();
        }

        // 13. Import Sales
        public static string ImportSales(CarDealerContext context, string inputXml)
        {
            //InitializeMapper();
            var serializer = new XmlSerializer(typeof(ImportSalesDto[]), new XmlRootAttribute("Sales"));

            using StringReader strr = new StringReader(inputXml);
            var salesDto = (ImportSalesDto[])serializer.Deserialize(strr);

            var cars = context.Cars.Select(x => x.Id).ToList();

            var sales = salesDto
                .Where(x => cars.Contains(x.CarId))
                .Select(x => new Sale
            {
                CarId = x.CarId,
                CustomerId = x.CustomerId,
                Discount = x.Discount
            })
                .ToList();

            context.AddRange(sales);
            context.SaveChanges();

            return $"Successfully imported {sales.Count()}";
        }

        // 12. Import Customers
        public static string ImportCustomers(CarDealerContext context, string inputXml)
        {
            InitializeMapper();
            var serializer = new XmlSerializer(typeof(ImportCustomersDto[]), new XmlRootAttribute("Customers"));

            using StringReader strr = new StringReader(inputXml);
            var customersDto = serializer.Deserialize(strr);

            var customers = mapper.Map<Customer[]>(customersDto);

            context.AddRange(customers);
            context.SaveChanges();

            return $"Successfully imported {customers.Count()}";
        }

        //11. Import Cars
        public static string ImportCars(CarDealerContext context, string inputXml)
        {
            //InitializeMapper();
            var serializer = new XmlSerializer(typeof(ImportCarsDto[]), new XmlRootAttribute("Cars"));

            using StringReader strr = new StringReader(inputXml);
            var carDtos = (ImportCarsDto[])serializer.Deserialize(strr);

            var allPrts = context.Parts.Select(x => x.Id).ToList();

            //var cars = new List<Car>();

            //foreach (var item in carDtos)
            //{
            //    var car = new Car
            //    {
            //        Make = item.Make,
            //        Model = item.Model,
            //        TravelledDistance = item.TravelledDistance
            //    };

            //    foreach (var carPart in item.PartCars.Distinct())
            //    {
            //        var part = new PartCar
            //        {
            //            Car = car,
            //            Part = allPrts.FirstOrDefault(p => p.Id == carPart.Id)
            //        };

            //        car.PartCars.Add(part);
            //    }

            //    cars.Add(car);
            //}

            var cars = carDtos.Select(c => new Car
            {
                Make = c.Make,
                Model = c.Model,
                TravelledDistance = c.TravelledDistance,
                PartCars = c.PartCars.Select(pc => pc.Id)
               .Distinct()
               .Intersect(allPrts)
               .Select(pc => new PartCar
               {
                   PartId = pc
               }).ToList()
            })
           .ToList();

         
            context.AddRange(cars);
            context.SaveChanges(); 

            return $"Successfully imported {cars.Count()}";
        }

        //10. Import Parts
        public static string ImportParts(CarDealerContext context, string inputXml)
        {
            InitializeMapper();
            var serializer = new XmlSerializer(typeof(ImportPartsDto[]), new XmlRootAttribute("Parts"));

            var partsDto = serializer.Deserialize(new StringReader(inputXml));

            var parts = mapper.Map<Part[]>(partsDto).Where(x => x.SupplierId < 32);

            context.AddRange(parts);
            context.SaveChanges();

            return $"Successfully imported {parts.Count()}";
        }

        //09. Import Suppliers
        public static string ImportSuppliers(CarDealerContext context, string inputXml)
        {
            InitializeMapper();
            XmlSerializer serializer = new XmlSerializer(typeof(ImportSuppliersDto[]), new XmlRootAttribute("Suppliers"));

            var dtos = serializer.Deserialize(new StringReader(inputXml));

            var suppliers = mapper.Map<List<Supplier>>(dtos);

            //var suppliers = dtos.Select(x => new Supplier
            //{
            //    Name = x.Name,
            //    IsImporter = x.IsImporter
            //})
            //.ToList();

            context.AddRange(suppliers);
            context.SaveChanges();

            return $"Successfully imported {suppliers.Count}";
        }

        private static void InitializeMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(typeof(CarDealerProfile));
            });
            mapper = config.CreateMapper();
        }
    }
}