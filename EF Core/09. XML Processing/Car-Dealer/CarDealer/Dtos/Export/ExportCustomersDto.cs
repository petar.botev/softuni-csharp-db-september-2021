﻿using System.Xml.Serialization;

namespace CarDealer.Dtos.Export
{
    [XmlType("customer")]
    public class ExportCustomersDto
    {
        [XmlAttribute("full-name")]
        public string Name { get; set; }

        [XmlAttribute("bought-cars")]
        public int Cars { get; set; }

        [XmlAttribute("spent-money")]
        public decimal TotalSpentMoney { get; set; }
    }
}
