﻿using AutoMapper;
using ProductShop.Data;
using ProductShop.Dtos.Export;
using ProductShop.Dtos.Import;
using ProductShop.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ProductShop
{
    public class StartUp
    {
        private static IMapper mapper;

        public static void Main(string[] args)
        {
            var db = new ProductShopContext();
            //string input = File.ReadAllText(@"..\..\..\Datasets\categories-products.xml");
            var res = GetUsersWithProducts(db);
            File.WriteAllText("../../../datasets/results/exportProducts.xml", res);
            //System.Console.WriteLine(GetProductsInRange(db));
        }

        //08. Export Users and Products
        public static string GetUsersWithProducts(ProductShopContext context)
        {
            var users = context.Users
                .ToList()
                .Where(u => u.ProductsSold.Count > 0)
                .OrderByDescending(u => u.ProductsSold.Count)
                .Select(u => new ExportUserDto
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Age = u.Age,
                    SoldProduct = new ExportProductCountDto
                    {
                        Count = u.ProductsSold.Count(),
                        Products = u.ProductsSold.Select(p => new ExportProdDto
                        {
                            Name = p.Name,
                            Price = p.Price
                        })
                        .OrderByDescending(x => x.Price)
                        .ToList()
                    }
                })
                .ToList();

            var result = new ExportUserCountDto
            {
                Count = users.Count(),
                Users = users.Take(10).ToList()
            };

            XmlSerializer serializer = new XmlSerializer(typeof(ExportUserCountDto), new XmlRootAttribute("Users"));

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            var stream = new StringWriter();

            serializer.Serialize(stream, result, ns);

            return stream.ToString();
        }

        //07. Export Categories By Products Count
        public static string GetCategoriesByProductsCount(ProductShopContext context)
        {
            var categories = context.Categories
                .Select(x => new ExportCategoriesDto
                {
                    Name = x.Name,
                    NumberOfProducts = x.CategoryProducts.Count(),
                    AvgPrice = x.CategoryProducts.Average(cp => cp.Product.Price),
                    TotalRevenue = x.CategoryProducts.Sum(cp => cp.Product.Price)
                })
                .OrderByDescending(x => x.NumberOfProducts)
                .ThenBy(x => x.TotalRevenue)
                .ToList();

            XmlSerializer serializer = new XmlSerializer(typeof(List<ExportCategoriesDto>), new XmlRootAttribute("Categories"));

            var stream = new StringWriter();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            serializer.Serialize(stream, categories, ns);

            return stream.ToString();
        }

        //06. Export Sold Products
        public static string GetSoldProducts(ProductShopContext context)
        {
            StringBuilder strb = new StringBuilder();
            var users = context.Users
                .Where(x => x.ProductsSold.Count() > 0)
                .Select(x => new ExportUsersDto
                {
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    ProductsSold = x.ProductsSold.Select(ps => new ExportSoldProductsDto
                    {
                        Name = ps.Name,
                        Price = ps.Price
                    }).ToList()
                })
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.FirstName)
                .Take(5)
                .ToList();

            XmlSerializer serializer = new XmlSerializer(typeof(List<ExportUsersDto>), new XmlRootAttribute("Users"));

            var stream = new StringWriter(strb);

            var namesmaces = new XmlSerializerNamespaces();
            namesmaces.Add("", "");

            serializer.Serialize(stream, users, namesmaces);

            return strb.ToString();
        }

        //05. Export Products In Range
        public static string GetProductsInRange(ProductShopContext context)
        {
            var products = context.Products
               .Where(p => p.Price >= 500 && p.Price <= 1000)
               .Select(p => new ExportProductDto
               {
                   Name = p.Name,
                   Price = p.Price,
                   BuyerFullName = $"{p.Buyer.FirstName} {p.Buyer.LastName}"
               })
               .OrderBy(p => p.Price)
               .Take(10)
               .ToList();

            XmlSerializer xsSubmit = new XmlSerializer(typeof(List<ExportProductDto>), new XmlRootAttribute("Products"));

            var sw = new StringWriter();

            var namesmaces = new XmlSerializerNamespaces();
            namesmaces.Add("", "");

            xsSubmit.Serialize(sw, products, namesmaces);

            return sw.ToString().TrimEnd();
        }

        //04. Import Categories and Products
        public static string ImportCategoryProducts(ProductShopContext context, string inputXml)
        {
            InitializeMapper();
            XmlSerializer serializer = new XmlSerializer(typeof(ImportCategoryProductDto[]), new XmlRootAttribute("CategoryProducts"));

            var categoryProductsDto = (ImportCategoryProductDto[])serializer.Deserialize(new StringReader(inputXml));
            var categoryProducts = mapper.Map<CategoryProduct[]>(categoryProductsDto);
            var categoriesToAdd = new List<CategoryProduct>();
            var categories = context.Categories.ToList();
            var products = context.Products.ToList();

            foreach (var item in categoryProducts)
            {
                if (categories.Any(x => x.Id == item.CategoryId) && products.Any(x => x.Id == item.ProductId))
                {
                    categoriesToAdd.Add(item);
                }
            }

            context.AddRange(categoriesToAdd);
            context.SaveChanges();

            return $"Successfully imported {categoryProducts.Count()}";
        }

        //03. Import Categories
        public static string ImportCategories(ProductShopContext context, string inputXml)
        {
            InitializeMapper();
            XmlSerializer serializer = new XmlSerializer(typeof(ImportCategoryDto[]), new XmlRootAttribute("Categories"));

            var categoriesDto = serializer.Deserialize(new StringReader(inputXml));
            var categories = mapper.Map<Category[]>(categoriesDto).Where(x => x.Name != null).ToList();

            context.AddRange(categories);
            var res = context.SaveChanges();

            return $"Successfully imported {res}";

        }

        //02. Import Products
        public static string ImportProducts(ProductShopContext context, string inputXml)
        {
            InitializeMapper();
            XmlSerializer serializer = new XmlSerializer(typeof(ImportProductDto[]), new XmlRootAttribute("Products"));

            var productDto = serializer.Deserialize(new StringReader(inputXml));
            var products = mapper.Map<Product[]>(productDto);

            context.AddRange(products);
            var res = context.SaveChanges();

            return $"Successfully imported {res}";
        }

        //01. Import Users
        public static string ImportUsers(ProductShopContext context, string inputXml)
        {
            InitializeMapper();
            XmlSerializer serializer = new XmlSerializer(typeof(ImportUserDto[]), new XmlRootAttribute("Users"));

            var usersDto = serializer.Deserialize(new StringReader(inputXml));

            var users = mapper.Map<User[]>(usersDto);

            context.AddRange(users);
            var res = context.SaveChanges();

            return $"Successfully imported {res}";
        }

        private static void InitializeMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(typeof(ProductShopProfile));
            });
            mapper = config.CreateMapper();
        }
    }
}