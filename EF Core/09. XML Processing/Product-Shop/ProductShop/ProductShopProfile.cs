﻿using AutoMapper;
using ProductShop.Dtos.Export;
using ProductShop.Dtos.Import;
using ProductShop.Models;

namespace ProductShop
{
    public class ProductShopProfile : Profile
    {
        public ProductShopProfile()
        {
            this.CreateMap<ImportUserDto, User>();
            this.CreateMap<ImportProductDto, Product>();
            this.CreateMap<ImportCategoryDto, Category>();
            this.CreateMap<ImportCategoryProductDto, CategoryProduct>();
            this.CreateMap<Product, ExportProductDto>()
                .ForMember(x => x.BuyerFullName, y => y.MapFrom(z => (z.Buyer.FirstName + " " + z.Buyer.LastName)))
                .ForMember(x => x.Price, y => y.MapFrom(z => z.Price / 1.00000000000000000000000m));
        }
    }
}
