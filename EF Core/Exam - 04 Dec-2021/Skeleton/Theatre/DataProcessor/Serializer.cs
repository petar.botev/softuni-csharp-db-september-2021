﻿namespace Theatre.DataProcessor
{
    using Newtonsoft.Json;
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using Theatre.Data;
    using Theatre.DataProcessor.ExportDto;

    public class Serializer
    {
        public static string ExportTheatres(TheatreContext context, int numbersOfHalls)
        {
            var minimumTickets = 20;
            var firstRowTicket = 1;
            var fifthRowTicket = 5;

            var theatresDtos = context.Theatres
                .ToArray()
                .Where(t => t.NumberOfHalls >= numbersOfHalls && t.Tickets.Count >= minimumTickets)
                .Select(t => new
                {
                    Name = t.Name,
                    Halls = t.NumberOfHalls,
                    TotalIncome = t.Tickets.Where(ti => ti.RowNumber >= firstRowTicket && ti.RowNumber <= fifthRowTicket).Sum(ti => ti.Price),
                    Tickets = t.Tickets
                        .Where(ti => ti.RowNumber >= firstRowTicket && ti.RowNumber <= fifthRowTicket)
                        .Select(ti => new
                        {
                            Price = ti.Price,
                            RowNumber = ti.RowNumber
                        })
                        .OrderByDescending(ti => ti.Price)
                        .ToArray()
                })
                .OrderByDescending(t => t.Halls)
                .ThenBy(t => t.Name)
                .ToArray();

            var theatres = JsonConvert.SerializeObject(theatresDtos, Formatting.Indented);

            return theatres;
        }

        public static string ExportPlays(TheatreContext context, double rating)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(PlayDto[]), new XmlRootAttribute("Plays"));

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            var playDtos = context.Plays
                .ToArray()
                .Where(p => p.Rating <= rating)
                .OrderBy(p => p.Title)
                .ThenByDescending(p => p.Genre)
                .Select(p => new PlayDto
                {
                    Title = p.Title,
                    Duration = p.Duration.ToString("c"),
                    Rating = p.Rating == 0 ? "Premier" : p.Rating.ToString(),
                    Genre = p.Genre.ToString(),
                    Actors = p.Casts
                        .Where(a => a.IsMainCharacter == true)
                        .Select(a => new ActorDto
                        {
                            FullName = a.FullName,
                            MainCharacter = $"Plays main character in '{a.Play.Title}'."
                        })
                        .OrderByDescending(a => a.FullName)
                        .ToArray()

                })
                .ToArray();

            using StringWriter strr = new StringWriter();
            serializer.Serialize(strr, playDtos, ns);

            return strr.ToString().TrimEnd();
        }
    }
}
