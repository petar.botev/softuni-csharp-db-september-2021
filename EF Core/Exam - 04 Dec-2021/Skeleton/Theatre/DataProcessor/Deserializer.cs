﻿namespace Theatre.DataProcessor
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;
    using Theatre.Data;
    using Theatre.Data.Models;
    using Theatre.Data.Models.Enums;
    using Theatre.DataProcessor.ImportDto;

    public class Deserializer
    {
        private const string ErrorMessage = "Invalid data!";

        private const string SuccessfulImportPlay
            = "Successfully imported {0} with genre {1} and a rating of {2}!";

        private const string SuccessfulImportActor
            = "Successfully imported actor {0} as a {1} character!";

        private const string SuccessfulImportTheatre
            = "Successfully imported theatre {0} with #{1} tickets!";

        public static string ImportPlays(TheatreContext context, string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(PlayDto[]), new XmlRootAttribute("Plays"));

            using StringReader strr = new StringReader(xmlString);
            var playDtos = (PlayDto[])serializer.Deserialize(strr);

            StringBuilder strb = new StringBuilder();

            List<Play> plays = new List<Play>();

            foreach (var playDto in playDtos)
            {
                if (!IsValid(playDto))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var isGenreValid = Enum.TryParse(typeof(Genre), playDto.Genre, out object newGenre);

                if (!isGenreValid)
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var isTimeSpanValid = TimeSpan.TryParseExact(playDto.Duration, "c", CultureInfo.InvariantCulture, out TimeSpan newDuration);

                if (!isTimeSpanValid)
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                TimeSpan oneHour = new TimeSpan(1, 0, 0);

                if (newDuration < oneHour)
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var play = new Play
                {
                    Title = playDto.Title,
                    Duration = newDuration,
                    Rating = playDto.Rating,
                    Genre = (Genre)newGenre,
                    Description = playDto.Description,
                    Screenwriter = playDto.Screenwriter
                };

                plays.Add(play);

                strb.AppendLine(string.Format(SuccessfulImportPlay, play.Title, play.Genre.ToString(), play.Rating));
            }

            context.AddRange(plays);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }

        public static string ImportCasts(TheatreContext context, string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CastDto[]), new XmlRootAttribute("Casts"));

            using StringReader strr = new StringReader(xmlString);
            var castDtos = (CastDto[])serializer.Deserialize(strr);

            StringBuilder strb = new StringBuilder();

            List<Cast> casts = new List<Cast>();

            foreach (var castDto in castDtos)
            {
                if (!IsValid(castDto))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var cast = new Cast
                {
                    FullName = castDto.FullName,
                    IsMainCharacter = castDto.IsMainCharacter,
                    PhoneNumber = castDto.PhoneNumber,
                    PlayId = castDto.PlayId
                };

                casts.Add(cast);

                strb.AppendLine(string.Format(SuccessfulImportActor, cast.FullName, cast.IsMainCharacter == true ? "main" : "lesser"));
            }

            context.AddRange(casts);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }

        public static string ImportTtheatersTickets(TheatreContext context, string jsonString)
        {
            var theatreDtos = JsonConvert.DeserializeObject<TheatreDto[]>(jsonString);

            StringBuilder strb = new StringBuilder();

            List<Theatre> theatres = new List<Theatre>();

            foreach (var theatreDto in theatreDtos)
            {
                if (!IsValid(theatreDto))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var theatre = new Theatre
                {
                    Name = theatreDto.Name,
                    NumberOfHalls = theatreDto.NumberOfHalls,
                    Director = theatreDto.Director,
                };

                //bool isPlayValid = true;
                foreach (var ticketDto in theatreDto.Tickets)
                {
                    if (!IsValid(ticketDto))
                    {
                        strb.AppendLine(ErrorMessage);
                        continue;
                    }

                    //var existingPlay = context.Plays.FirstOrDefault(p => p.Id == ticketDto.PlayId);

                    //if (existingPlay == null)
                    //{
                    //    isPlayValid = false;
                    //    break;
                    //}

                    theatre.Tickets.Add(new Ticket
                    {
                        Price = ticketDto.Price,
                        RowNumber = ticketDto.RowNumber,
                        PlayId = ticketDto.PlayId
                    });
                }

                //if (!isPlayValid)
                //{
                //    strb.AppendLine(ErrorMessage);
                //    continue;
                //}

                theatres.Add(theatre);

                strb.AppendLine(string.Format(SuccessfulImportTheatre, theatre.Name, theatre.Tickets.Count));
            }

            context.AddRange(theatres);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }


        private static bool IsValid(object obj)
        {
            var validator = new ValidationContext(obj);
            var validationRes = new List<ValidationResult>();

            var result = Validator.TryValidateObject(obj, validator, validationRes, true);
            return result;
        }
    }
}
