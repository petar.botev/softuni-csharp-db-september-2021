﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Theatre.DataProcessor.ExportDto
{
    [XmlType("Play")]
    public class PlayDto
    {
        [Required]
        [StringLength(50, MinimumLength = 4)]
        [XmlAttribute("Title")]
        public string Title { get; set; }

        [Required]
        [XmlAttribute("Duration")]
        public string Duration { get; set; }

        [Required]
        [Range(0.00, 10.00)]
        [XmlAttribute("Rating")]
        public string Rating { get; set; }

        [Required]
        [XmlAttribute("Genre")]
        public string Genre { get; set; }

        [XmlArray("Actors")]
        public ActorDto[] Actors { get; set; }
    }
}
