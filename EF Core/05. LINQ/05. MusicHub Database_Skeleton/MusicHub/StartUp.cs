﻿namespace MusicHub
{
    using System;
    using System.Linq;
    using System.Text;
    using Data;
    using Initializer;

    public class StartUp
    {
        public static void Main(string[] args)
        {
            MusicHubDbContext context = 
                new MusicHubDbContext();

            DbInitializer.ResetDatabase(context);

            //Test your solutions here
            Console.WriteLine(ExportSongsAboveDuration(context, 4));
        }

        public static string ExportAlbumsInfo(MusicHubDbContext context, int producerId)
        {
            var albums = context.Albums
                .ToList()
                .Where(x => x.ProducerId == producerId)
                .Select(x => new
                {
                    x.Name,
                    ReleaseDate = x.ReleaseDate,
                    ProducerName = x.Producer.Name,
                    TotalAlbumPrice = x.Songs.Sum(s => s.Price),
                    Songs = x.Songs.Select(so => new 
                    {   SongName = so.Name, 
                        SongPrice = so.Price,
                        WriterName = so.Writer.Name 
                    })
                        .OrderByDescending(s => s.SongName)
                        .ThenBy(s => s.WriterName)
                        .ToList()
                })
                .OrderByDescending(a => a.TotalAlbumPrice);
                

            StringBuilder strb = new StringBuilder();
            
            foreach (var album in albums)
            {
                int counter = 1;
                strb.AppendLine($"-AlbumName: {album.Name}");
                strb.AppendLine($"-ReleaseDate: {album.ReleaseDate.ToString("MM/dd/yyyy")}");
                strb.AppendLine($"-ProducerName: {album.ProducerName}");
                strb.AppendLine($"-Songs:");

                foreach (var song in album.Songs)
                {
                    strb.AppendLine($"---#{counter}");
                    strb.AppendLine($"---SongName: {song.SongName}");
                    strb.AppendLine($"---Price: {song.SongPrice:f2}");
                    strb.AppendLine($"---Writer: {song.WriterName}");
                    counter++;
                }
                strb.AppendLine($"-AlbumPrice: {album.TotalAlbumPrice:f2}");
            }

            return strb.ToString().Trim();
        }

        public static string ExportSongsAboveDuration(MusicHubDbContext context, int duration)
        {

            var songs = context.Songs
                .ToList()
                .Where(s => s.Duration.TotalSeconds > duration)
                .Select(s => new
                {
                    s.Name,
                    PerformerFullName = s.SongPerformers.Select(sp => new
                    {
                        Name = $"{sp.Performer.FirstName} {sp.Performer.LastName}",
                    }).FirstOrDefault(),
                    WriterName = s.Writer.Name,
                    AlbumProducer = s.Album.Producer.Name,
                    Duration = s.Duration.ToString("c")
                })
                .OrderBy(s => s.Name)
                .ThenBy(s => s.WriterName)
                .ThenBy(s => s.PerformerFullName)
                .ToList()
               ;

            StringBuilder strb = new StringBuilder();
            
            int counter = 1;
            foreach (var song in songs)
            {
                //StringBuilder strb2 = new StringBuilder();
                
                //    strb2.Append($"{song.PerformerFullNames.ToList()}");
                
                strb.AppendLine($"-Song #{counter}");
                strb.AppendLine($"---SongName: {song.Name}");
                strb.AppendLine($"---Writer: {song.WriterName}");
                if (song.PerformerFullName == null)
                {
                    strb.AppendLine($"---Performer: ");
                }
                else
                {
                    strb.AppendLine($"---Performer: {song.PerformerFullName.Name}");
                }
                strb.AppendLine($"---AlbumProducer: {song.AlbumProducer}");
                strb.AppendLine($"---Duration: {song.Duration}");

                counter++;
            }

            return strb.ToString().Trim();
        }
    }
}
