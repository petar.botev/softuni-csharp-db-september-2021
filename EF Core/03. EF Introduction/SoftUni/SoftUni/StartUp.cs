﻿using Microsoft.EntityFrameworkCore;
using SoftUni.Data;
using SoftUni.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftUni
{
    public class StartUp
    {
        static void Main(string[] args)
        {
            var context = new SoftUniContext();
           
            Console.WriteLine(RemoveTown(context));
        }

        //03.Employees Full Information
        public static string GetEmployeesFullInformation(SoftUniContext context)
        {
            var employees = context.Employees.Select(e => new { 
                     Id = e.EmployeeId,
                     FirstName = e.FirstName, 
                     LastName = e.LastName, 
                     e.MiddleName, 
                     e.JobTitle, 
                     Salary = Math.Round(e.Salary, 2)})
                .OrderBy(e => e.Id)
                .ToList();
            StringBuilder strb = new StringBuilder();
            foreach (var item in employees)
            {
                strb.AppendLine($"{item.FirstName} {item.LastName} {item.MiddleName} {item.JobTitle} {item.Salary:0.00}");
            }

            return strb.ToString();
        }

        //04.Employees with Salary Over 50 000
        public static string GetEmployeesWithSalaryOver50000(SoftUniContext context)
        {
            var employees = context.Employees
                .Select(e => new
                {
                    e.FirstName,
                    e.Salary
                })
                .Where(e => e.Salary > 50000)
                .OrderBy(e => e.FirstName)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var item in employees)
            {
                strb.AppendLine($"{item.FirstName} - {Math.Round(item.Salary, 2):0.00}");
            }
            return strb.ToString();
        }

        //05. Employees from Research and Development
        public static string GetEmployeesFromResearchAndDevelopment(SoftUniContext context)
        {
            var emplayees = context.Employees
                .Select(e => new
                {
                    e.FirstName,
                    e.LastName,
                    e.Department.Name,
                    e.Salary
                })
                .Where(e => e.Name == "Research and Development")
                .OrderBy(e => e.Salary)
                .ThenByDescending(e => e.FirstName)
                .ToList();

            StringBuilder strb = new StringBuilder();
            foreach (var item in emplayees)
            {
                strb.AppendLine($"{item.FirstName} {item.LastName} from {item.Name} - ${item.Salary:0.00}");
            }
            return strb.ToString();
        }

        //06. Adding a New Address and Updating Employee
        public static string AddNewAddressToEmployee(SoftUniContext context)
        {
            var employee = context.Employees
                .FirstOrDefault(e => e.LastName == "Nakov");

            var newAddress = new Address()
            {
                AddressText = "Vitoshka 15",
                TownId = 4
            };

            employee.Address = newAddress;
            context.SaveChanges();

            var result = context.Employees
                .Select(e => new
                {
                    e.AddressId,
                    e.Address.AddressText
                })
                .OrderByDescending(e => e.AddressId)
                .Take(10);

            StringBuilder strb = new StringBuilder();
            foreach (var item in result)
            {
                strb.AppendLine($"{item.AddressText}");
            }
            return strb.ToString();
        }

        //07.Employees and Projects
        public static string GetEmployeesInPeriod(SoftUniContext context)
        {
            var employees = context.Employees
                .Include(e => e.Manager)
                .Include(e => e.EmployeesProjects)
                .ThenInclude(ep => ep.Project)
                .Where(x => x.EmployeesProjects.Any(ep => ep.Project.StartDate.Year >= 2001 && ep.Project.StartDate.Year <= 2003))
                .Take(10)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var employee in employees)
            {
                strb.AppendLine($"{employee.FirstName} {employee.LastName} - Manager: {employee.Manager.FirstName} {employee.Manager.LastName}");
                foreach (var ep in employee.EmployeesProjects)
                {
                    if (ep.Project.EndDate == null)
                    {
                        strb.AppendLine($"--{ep.Project.Name} - {ep.Project.StartDate} - not finished");
                    }
                    else
                    {
                        strb.AppendLine($"--{ep.Project.Name} - {ep.Project.StartDate} - {ep.Project.EndDate}");
                    }
                }
            }
            return strb.ToString();
        }

        //08. Addresses by Town
        public static string GetAddressesByTown(SoftUniContext context)
        {
            var addresses = context.Addresses
                .Include(a => a.Town)
                .Include(a => a.Employees)
                .OrderByDescending(a => a.Employees.Count)
                .ThenBy(a => a.Town.Name)
                .ThenBy(a => a.AddressText)
                .Take(10)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var address in addresses)
            {
                strb.AppendLine($"{address.AddressText}, {address.Town.Name} - {address.Employees.Count} employees");
            }

            return strb.ToString();
        }

        //09. Employee 147
        public static string GetEmployee147(SoftUniContext context)
        {
            var employee = context.Employees
                .Select(e => new
                {
                    e.EmployeeId,
                    e.FirstName,
                    e.LastName,
                    e.JobTitle,
                    ProjectNames = e.EmployeesProjects.OrderBy(ep => ep.Project.Name).Select(ep => ep.Project.Name)
                })
                .FirstOrDefault(e => e.EmployeeId == 147);
                
            StringBuilder strb = new StringBuilder();
            strb.AppendLine($"{employee.FirstName} {employee.LastName} - {employee.JobTitle}");

            foreach (var name in employee.ProjectNames)
            {
                strb.AppendLine(name);
            }
            
            return strb.ToString().Trim();
        }

        //10. Departments with More Than 5 Employees
        public static string GetDepartmentsWithMoreThan5Employees(SoftUniContext context)
        {
            var departments = context.Departments
                .Include(d => d.Employees)
                .Include(d => d.Manager)
                .Where(d => d.Employees.Count > 5)
                .OrderBy(x => x.Employees.Count)
                .ThenBy(x => x.Name)
                .Select(d => new
                {
                    DepartmentName = d.Name,
                    ManagerFirstName = d.Manager.FirstName,
                    ManagerLastName = d.Manager.LastName,
                    Employees = d.Employees
                              .OrderBy(e => e.FirstName)
                              .ThenBy(e => e.LastName)
                              .ToList()
                })
                .ToList();

            StringBuilder strb = new StringBuilder();
            foreach (var department in departments)
            {
                strb.AppendLine($"{department.DepartmentName} - {department.ManagerFirstName} {department.ManagerLastName}");

                foreach (var employee in department.Employees)
                {
                    strb.AppendLine($"{employee.FirstName} {employee.LastName} - {employee.JobTitle}");
                }
            }
            
            return strb.ToString().Trim();
        }

        //11. Find Latest 10 Projects
        public static string GetLatestProjects(SoftUniContext context)
        {
            var projects = context.Projects
                .OrderByDescending(p => p.StartDate)
                .Take(10)
                .OrderBy(p => p.Name)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var project in projects)
            {
                strb.AppendLine(project.Name);
                strb.AppendLine(project.Description);
                strb.AppendLine(project.StartDate.ToString("M/d/yyyy h:mm:ss tt"));
            }

            return strb.ToString().Trim();
        }

        //12. Increase Salaries
        public static string IncreaseSalaries(SoftUniContext context)
        {
            var deps = new List<string> { "Engineering", "Tool Design", "Marketing", "Information Services" };

            var employeesToIncrease = context.Employees
                .Where(e => deps.Contains(e.Department.Name))
                .OrderBy(e => e.FirstName)
                .ThenBy(e => e.LastName)
                .ToList();
            //.ForEach(e => { e.Salary += e.Salary * 12});

            foreach (var employee in employeesToIncrease)
            {
                employee.Salary += employee.Salary * 0.12M; 
            }

            context.SaveChanges();

            StringBuilder strb = new StringBuilder();
            foreach (var employee in employeesToIncrease)
            {
                strb.AppendLine($"{employee.FirstName} {employee.LastName} (${employee.Salary:f2})");
            }

            return strb.ToString().Trim();
        }

        //13. Find Employees by First Name Starting With Sa
        public static string GetEmployeesByFirstNameStartingWithSa(SoftUniContext context)
        {
            var employees = context.Employees
                .Where(e => e.FirstName.ToLower().StartsWith("sa"))
                .OrderBy(e => e.FirstName)
                .ThenBy(e => e.LastName)
                .ToList();

            StringBuilder strb = new StringBuilder();
            foreach (var employee in employees)
            {
                strb.AppendLine($"{employee.FirstName} {employee.LastName} - {employee.JobTitle} - (${employee.Salary:f2})");
            }

            return strb.ToString().Trim();
        }

        //14. Delete Project by Id
        public static string DeleteProjectById(SoftUniContext context)
        {
            var projectToDelete = context.Projects.Find(2);
            var projEmplToDelete = context.EmployeesProjects
                .Where(ep => ep.Project.ProjectId == 2)
                .ToList();

            context.EmployeesProjects.RemoveRange(projEmplToDelete);
            context.Projects.Remove(projectToDelete);

            var projectNames = context.Projects
                .Take(10)
                .ToList();

            StringBuilder strb = new StringBuilder();
            foreach (var project in projectNames)
            {
                strb.AppendLine(project.Name);
            }

            return strb.ToString().Trim();
        }

        //15. Remove Town
        public static string RemoveTown(SoftUniContext context)
        {
            var empFromSeattle = context.Employees
                .Include(e => e.Address)
                .Where(e => e.Address.Town.Name == "Seattle")
                .ToList();
            foreach (var emp in empFromSeattle)
            {
                emp.Address.TownId = null;
            }

            var addresses = context.Addresses
                .Where(a => a.Town.Name == "Seattle")
                .ToList();

            var addressCOunt = addresses.Count;

            foreach (var address in addresses)
            {
                context.Remove(address);
                //address.TownId = null;
            }

            var town = context.Towns
                .FirstOrDefault(t => t.Name == "Seattle");

            context.Remove(town);
            context.SaveChanges();

            return $"{addressCOunt} addresses in Seattle were deleted";
        }
    }
}
