﻿using BookShop.Data;
using System.Linq;
using System.Text;

namespace BookShop.Solutions
{
    public static class _01AgeRestriction
    {
        public static string GetBooksByAgeRestriction(BookShopContext context, string command)
        {
            var books = context.Books
                .Where(b => b.AgeRestriction != 0)
                .OrderBy(b => b.Title)
                .Select(b => b.Title)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var book in books)
            {
                strb.AppendLine(book);
            }

            return strb.ToString();
        }
    }
}
