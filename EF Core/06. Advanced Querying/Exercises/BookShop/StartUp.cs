﻿namespace BookShop
{
    using BookShop.Models.Enums;
    using BookShop.Solutions;
    using Data;
    using Initializer;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class StartUp
    {
        public static void Main()
        {
            using var db = new BookShopContext();
            //DbInitializer.ResetDatabase(db);

            //var input = int.Parse(Console.ReadLine());

            Console.WriteLine(RemoveBooks(db));
        }

        //2. Age Restriction
        public static string GetBooksByAgeRestriction(BookShopContext context, string command)
        {
            var restriction = Enum.Parse<AgeRestriction>(command, true);
            var books = context.Books
                .Where(b => b.AgeRestriction == restriction)
                .OrderBy(b => b.Title)
                .Select(b => b.Title)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var book in books)
            {
                strb.AppendLine(book);
            }

            return strb.ToString();
        }

        //3. Golden Books
        public static string GetGoldenBooks(BookShopContext context)
        {
            var books = context.Books
                .Where(b => b.EditionType == Enum.Parse<EditionType>("gold", true) && b.Copies < 5000)
                .OrderBy(b => b.BookId)
                .ToList();

            StringBuilder strb = new StringBuilder();
            foreach (var book in books)
            {
                strb.AppendLine(book.Title);
            }

            return strb.ToString().TrimEnd();
        }

        //4. Books by Price
        public static string GetBooksByPrice(BookShopContext context)
        {
            var books = context.Books
                .ToList()
                .Where(b => b.Price > 40)
                .Select(b => new
                {
                    b.Title,
                    b.Price
                })
                .OrderByDescending(b => b.Price)
                ;

            StringBuilder strb = new StringBuilder();
            foreach (var book in books)
            {
                strb.AppendLine($"{book.Title} - ${book.Price:f2}");
            }

            return strb.ToString();
        }

        //5. Not Released In
        public static string GetBooksNotReleasedIn(BookShopContext context, int year)
        {
            var books = context.Books
                .Where(b => b.ReleaseDate.Value.Year != year)
                .OrderBy(b => b.BookId)
                .Select(b => b.Title)
                .ToList();

            return string.Join(Environment.NewLine, books);
        }

        //6. Book Titles by Category
        public static string GetBooksByCategory(BookShopContext context, string input)
        {
            List<string> categories = input.Split(" ").ToList();

            var categoriesToLower = categories.Select(c => c.ToLower()).ToList();

            var books = context.Books
                .Select(b => new
                {
                    CategoryNames = b.BookCategories.Select(bc => bc.Category.Name),
                    b.Title
                })
                .Where(b => b.CategoryNames.Any(cn => categoriesToLower.Contains(cn.ToLower())))
                .OrderBy(b => b.Title)
                .ToList();

            StringBuilder strb = new StringBuilder();
            foreach (var book in books)
            {
                strb.AppendLine(book.Title);
            }

            return strb.ToString().TrimEnd();
        }

        //7. Released Before Date
        public static string GetBooksReleasedBefore(BookShopContext context, string date)
        {
            var dateParts = date.Split("-").ToList();
            dateParts.Reverse();
            var inputDate = string.Join("-", dateParts);
            
            var books = context.Books
                .Where(b => b.ReleaseDate < DateTime.Parse(inputDate))
                .Select(b => new
                {
                    b.Title,
                    b.EditionType,
                    b.Price,
                    b.ReleaseDate
                })
                .OrderByDescending(b => b.ReleaseDate)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var book in books)
            {
                strb.AppendLine($"{book.Title} - {book.EditionType} - ${book.Price:f2}");
            }

            return strb.ToString().TrimEnd();
        }

        //8. Author Search
        public static string GetAuthorNamesEndingIn(BookShopContext context, string input)
        {
            var authors = context.Authors
                .ToList()
                .Where(a => a.FirstName.EndsWith(input))
                .Select(a => new
                {
                    FullName = $"{a.FirstName} {a.LastName}"
                })
                .OrderBy(a => a.FullName)
                .ToList();
                
            return string.Join(Environment.NewLine, authors.Select(a => a.FullName));
        }

        //9. Book Search
        public static string GetBookTitlesContaining(BookShopContext context, string input)
        {
            var books = context.Books
                .Where(b => b.Title.ToLower().Contains(input.ToLower()))
                .OrderBy(b => b.Title)
                .ToList();

            return string.Join(Environment.NewLine, books.Select(b => b.Title));
        }

        //10. Book Search by Author
        public static string GetBooksByAuthor(BookShopContext context, string input)
        {
            var books = context.Books
                .Where(b => b.Author.LastName.ToLower().StartsWith(input.ToLower()))
                .Select(b => new
                {
                    Result = $"{b.Title} ({b.Author.FirstName} {b.Author.LastName})"
                })
                .ToList();

            return string.Join(Environment.NewLine, books.Select(b => b.Result));
        }

        //11. Count Books
        public static int CountBooks(BookShopContext context, int lengthCheck)
        {
            var books = context.Books
                .Where(b => b.Title.Length > lengthCheck)
                .ToList();

            return books.Count;
        }

        //12. Total Book Copies
        public static string CountCopiesByAuthor(BookShopContext context)
        {
            var authors = context.Authors
                .Select(a => new
                {
                    AuthorName = $"{a.FirstName} {a.LastName}",
                    BookCopies = a.Books.Sum(b => b.Copies)
                })
                .OrderByDescending(a => a.BookCopies)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var author in authors)
            {
                strb.AppendLine($"{author.AuthorName} - {author.BookCopies}");
            }

            return strb.ToString().TrimEnd();
        }

        //13. Profit by Category
        public static string GetTotalProfitByCategory(BookShopContext context)
        {
            var groupedBooks = context.Categories
                .Select(c => new
                {
                    c.Name,
                    Profit = c.CategoryBooks.Sum(cb => cb.Book.Price * cb.Book.Copies)
                })
                .OrderByDescending(x => x.Profit)
                .ThenBy(x => x.Name)
                .ToList();

            StringBuilder strb = new StringBuilder();
            foreach (var book in groupedBooks)
            {
                strb.AppendLine($"{book.Name} ${book.Profit:f2}");
            }
            
            return strb.ToString();
        }

        //14. Most Recent Books
        public static string GetMostRecentBooks(BookShopContext context)
        {
            var categories = context.Categories
                
                .Select(c => new
                {
                    c.Name,
                    
                    Books = c.CategoryBooks.Select(cb => new
                    {
                        BookName = cb.Book.Title,
                        Date = cb.Book.ReleaseDate

                    }).OrderByDescending(b => b.Date).Take(3).ToList()
                })
                .OrderBy(c => c.Name)
                .ToList();

            StringBuilder strb = new StringBuilder();

            foreach (var cat in categories)
            {
                strb.AppendLine($"--{cat.Name}");
                foreach (var book in cat.Books)
                {
                    strb.AppendLine($"{book.BookName} ({book.Date.Value.Year})");
                }
                
            }

            return strb.ToString().TrimEnd();
        }

        //15. Increase Prices
        public static void IncreasePrices(BookShopContext context)
        {
            var books = context.Books
                .Where(b => b.ReleaseDate < DateTime.Parse("2010-01-01"))
                .ToList();

            foreach (var book in books)
            {
                book.Price += 5.00M;
            }

            context.SaveChanges();
        }

        //16. Remove Books
        public static int RemoveBooks(BookShopContext context)
        {
            var books = context.Books
                .Where(b => b.Copies < 4200)
                .ToList();

            context.RemoveRange(books);

            var res = context.SaveChanges();

            return books.Count;
        }
    }
}
