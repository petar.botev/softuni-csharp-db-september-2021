﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace SoftJail.DataProcessor.ImportDto
{
    [XmlType("Prisoner")]
    public class ImportOfficerPrisonerDto
    {
        [ForeignKey("Officer")]
        [XmlAttribute("id")]
        public int PrisonerId { get; set; }
    }
}
