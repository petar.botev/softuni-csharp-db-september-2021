﻿namespace SoftJail.DataProcessor
{

    using Data;
    using Newtonsoft.Json;
    using SoftJail.Data.Models;
    using SoftJail.Data.Models.Enums;
    using SoftJail.DataProcessor.ImportDto;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    public class Deserializer
    {
        public static string ImportDepartmentsCells(SoftJailDbContext context, string jsonString)
        {
            var departmentDto = JsonConvert.DeserializeObject<ImportDepartmentDto[]>(jsonString);

            StringBuilder strb = new StringBuilder();

            List<Department> departments = new List<Department>();

            foreach (var dep in departmentDto)
            {
                if (!IsValid(dep))
                {
                    strb.AppendLine("Invalid Data");
                    continue;
                }

                var department = new Department
                {
                    Name = dep.Name
                };

                var isCellValid = true;

                foreach (var cellDto in dep.Cells)
                {
                    if (!IsValid(cellDto))
                    {
                        isCellValid = false;
                        break;
                    }

                    department.Cells.Add(new Cell
                    {
                        CellNumber = cellDto.CellNumber,
                        HasWindow = cellDto.HasWindow
                    });
                }

                if (!isCellValid)
                {
                    strb.AppendLine("Invalid Data");
                    continue;
                }

                departments.Add(department);

                strb.AppendLine($"Imported {department.Name} with {department.Cells.Count} cells");
            }

            context.Departments.AddRange(departments);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }

        public static string ImportPrisonersMails(SoftJailDbContext context, string jsonString)
        {
            var prisonersDto = JsonConvert.DeserializeObject<ImportPrisonerDto[]>(jsonString);

            StringBuilder strb = new StringBuilder();
            List<Prisoner> prisoners = new List<Prisoner>();

            foreach (var prisonerDto in prisonersDto)
            {
                if (!IsValid(prisonerDto))
                {
                    strb.AppendLine("Invalid Data");
                    continue;
                }

                var isIncarcerationDateValid = DateTime.TryParseExact(prisonerDto.IncarcerationDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime newIncarceration);

                if (!isIncarcerationDateValid)
                {
                    strb.AppendLine("Invalid Data");
                    continue;
                }

                var isReleaseDateValid = DateTime.TryParseExact(prisonerDto.ReleaseDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime newReleaseDate);

                //if (!isReleaseDateValid)
                //{
                //    strb.AppendLine("Invalid Data");
                //    continue;
                //}

                var newPrisoner = new Prisoner
                {
                    FullName = prisonerDto.FullName,
                    Nickname = prisonerDto.Nickname,
                    Age = prisonerDto.Age,
                    IncarcerationDate = newIncarceration,
                    ReleaseDate = newReleaseDate,
                    Bail = prisonerDto.Bail,
                    CellId = prisonerDto.CellId,
                };

                bool isMailValid = true;

                foreach (var mailDto in prisonerDto.Mails)
                {
                    if (!IsValid(mailDto))
                    {
                        isMailValid = false;
                        break;
                    }

                    newPrisoner.Mails.Add(new Mail
                    {
                        Description = mailDto.Description,
                        Sender = mailDto.Sender,
                        Address = mailDto.Address
                    });
                }

                if (!isMailValid)
                {
                    strb.AppendLine("Invalid Data");
                    continue;
                }

                prisoners.Add(newPrisoner);

                strb.AppendLine($"Imported {newPrisoner.FullName} {newPrisoner.Age} years old");
            }

            context.AddRange(prisoners);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }

        public static string ImportOfficersPrisoners(SoftJailDbContext context, string xmlString)
        {
            StringBuilder sb = new StringBuilder();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ImportOfficerDto[]), new XmlRootAttribute("Officers"));

            List<Officer> officers = new List<Officer>();

            using (StringReader stringReader = new StringReader(xmlString))
            {
                ImportOfficerDto[] officerDtos = (ImportOfficerDto[])xmlSerializer.Deserialize(stringReader);

                foreach (ImportOfficerDto officerDto in officerDtos)
                {
                    if (!IsValid(officerDto))
                    {
                        sb.AppendLine("Invalid Data");
                        continue;
                    }

                    object positionObj;
                    bool isPositionValid = Enum.TryParse(typeof(Position), officerDto.Position, out positionObj);

                    if (!isPositionValid)
                    {
                        sb.AppendLine("Invalid Data");
                        continue;
                    }

                    object weaponObj;
                    bool isWeaponValid = Enum.TryParse(typeof(Weapon), officerDto.Weapon, out weaponObj);

                    if (!isWeaponValid)
                    {
                        sb.AppendLine("Invalid Data");
                        continue;
                    }

                    Officer officer = new Officer()
                    {
                        FullName = officerDto.FullName,
                        Salary = officerDto.Salary,
                        Position = (Position)positionObj,
                        Weapon = (Weapon)weaponObj,
                        DepartmentId = officerDto.DepartmentId,
                    };

                    foreach (ImportOfficerPrisonerDto opDto in officerDto.Prisoners)
                    {
                        officer.OfficerPrisoners.Add(new OfficerPrisoner()
                        {
                            Officer = officer,
                            PrisonerId = opDto.PrisonerId
                        });
                    }

                    officers.Add(officer);

                    sb.AppendLine($"Imported {officer.FullName} ({officer.OfficerPrisoners.Count} prisoners)");
                }

                context.Officers.AddRange(officers);
                context.SaveChanges();
            }


            return sb.ToString().TrimEnd();
        }

        private static bool IsValid(object obj)
        {
            var validationContext = new System.ComponentModel.DataAnnotations.ValidationContext(obj);
            var validationResult = new List<ValidationResult>();

            bool isValid = Validator.TryValidateObject(obj, validationContext, validationResult, true);
            return isValid;
        }
    }
}