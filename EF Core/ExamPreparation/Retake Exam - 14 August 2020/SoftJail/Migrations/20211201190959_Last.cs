﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SoftJail.Migrations
{
    public partial class Last : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OfficerPrisoners_Officers_OfficerId",
                table: "OfficerPrisoners");

            migrationBuilder.DropForeignKey(
                name: "FK_OfficerPrisoners_Prisoners_PrisonerId",
                table: "OfficerPrisoners");

            migrationBuilder.DropForeignKey(
                name: "FK_Prisoners_Cells_CellId",
                table: "Prisoners");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OfficerPrisoners",
                table: "OfficerPrisoners");

            migrationBuilder.RenameTable(
                name: "OfficerPrisoners",
                newName: "OfficersPrisoners");

            migrationBuilder.RenameIndex(
                name: "IX_OfficerPrisoners_OfficerId",
                table: "OfficersPrisoners",
                newName: "IX_OfficersPrisoners_OfficerId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ReleaseDate",
                table: "Prisoners",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "CellId",
                table: "Prisoners",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<decimal>(
                name: "Bail",
                table: "Prisoners",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OfficersPrisoners",
                table: "OfficersPrisoners",
                columns: new[] { "PrisonerId", "OfficerId" });

            migrationBuilder.AddForeignKey(
                name: "FK_OfficersPrisoners_Officers_OfficerId",
                table: "OfficersPrisoners",
                column: "OfficerId",
                principalTable: "Officers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OfficersPrisoners_Prisoners_PrisonerId",
                table: "OfficersPrisoners",
                column: "PrisonerId",
                principalTable: "Prisoners",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Prisoners_Cells_CellId",
                table: "Prisoners",
                column: "CellId",
                principalTable: "Cells",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OfficersPrisoners_Officers_OfficerId",
                table: "OfficersPrisoners");

            migrationBuilder.DropForeignKey(
                name: "FK_OfficersPrisoners_Prisoners_PrisonerId",
                table: "OfficersPrisoners");

            migrationBuilder.DropForeignKey(
                name: "FK_Prisoners_Cells_CellId",
                table: "Prisoners");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OfficersPrisoners",
                table: "OfficersPrisoners");

            migrationBuilder.RenameTable(
                name: "OfficersPrisoners",
                newName: "OfficerPrisoners");

            migrationBuilder.RenameIndex(
                name: "IX_OfficersPrisoners_OfficerId",
                table: "OfficerPrisoners",
                newName: "IX_OfficerPrisoners_OfficerId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ReleaseDate",
                table: "Prisoners",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CellId",
                table: "Prisoners",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Bail",
                table: "Prisoners",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OfficerPrisoners",
                table: "OfficerPrisoners",
                columns: new[] { "PrisonerId", "OfficerId" });

            migrationBuilder.AddForeignKey(
                name: "FK_OfficerPrisoners_Officers_OfficerId",
                table: "OfficerPrisoners",
                column: "OfficerId",
                principalTable: "Officers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OfficerPrisoners_Prisoners_PrisonerId",
                table: "OfficerPrisoners",
                column: "PrisonerId",
                principalTable: "Prisoners",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Prisoners_Cells_CellId",
                table: "Prisoners",
                column: "CellId",
                principalTable: "Cells",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
