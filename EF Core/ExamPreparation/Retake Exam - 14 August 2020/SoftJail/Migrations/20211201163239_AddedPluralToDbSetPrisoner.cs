﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SoftJail.Migrations
{
    public partial class AddedPluralToDbSetPrisoner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mails_Prisoner_PrisonerId",
                table: "Mails");

            migrationBuilder.DropForeignKey(
                name: "FK_OfficerPrisoners_Prisoner_PrisonerId",
                table: "OfficerPrisoners");

            migrationBuilder.DropForeignKey(
                name: "FK_Prisoner_Cells_CellId",
                table: "Prisoner");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Prisoner",
                table: "Prisoner");

            migrationBuilder.RenameTable(
                name: "Prisoner",
                newName: "Prisoners");

            migrationBuilder.RenameIndex(
                name: "IX_Prisoner_CellId",
                table: "Prisoners",
                newName: "IX_Prisoners_CellId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Prisoners",
                table: "Prisoners",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Mails_Prisoners_PrisonerId",
                table: "Mails",
                column: "PrisonerId",
                principalTable: "Prisoners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OfficerPrisoners_Prisoners_PrisonerId",
                table: "OfficerPrisoners",
                column: "PrisonerId",
                principalTable: "Prisoners",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Prisoners_Cells_CellId",
                table: "Prisoners",
                column: "CellId",
                principalTable: "Cells",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mails_Prisoners_PrisonerId",
                table: "Mails");

            migrationBuilder.DropForeignKey(
                name: "FK_OfficerPrisoners_Prisoners_PrisonerId",
                table: "OfficerPrisoners");

            migrationBuilder.DropForeignKey(
                name: "FK_Prisoners_Cells_CellId",
                table: "Prisoners");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Prisoners",
                table: "Prisoners");

            migrationBuilder.RenameTable(
                name: "Prisoners",
                newName: "Prisoner");

            migrationBuilder.RenameIndex(
                name: "IX_Prisoners_CellId",
                table: "Prisoner",
                newName: "IX_Prisoner_CellId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Prisoner",
                table: "Prisoner",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Mails_Prisoner_PrisonerId",
                table: "Mails",
                column: "PrisonerId",
                principalTable: "Prisoner",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OfficerPrisoners_Prisoner_PrisonerId",
                table: "OfficerPrisoners",
                column: "PrisonerId",
                principalTable: "Prisoner",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Prisoner_Cells_CellId",
                table: "Prisoner",
                column: "CellId",
                principalTable: "Cells",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
