﻿namespace VaporStore.DataProcessor
{
	using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using Data;
    using Newtonsoft.Json;
    using VaporStore.Data.Models.Enums;
    using VaporStore.DataProcessor.Dto.Export;

    public static class Serializer
	{
		public static string ExportGamesByGenres(VaporStoreDbContext context, string[] genreNames)
		{

            var genres = context.Genres
                .ToList()
                .Where(g => genreNames.Contains(g.Name))
                .Select(g => new
                {
                    Id = g.Id,
                    Genre = g.Name,
                    Games = g.Games
                    .Where(game => game.Purchases.Any())
                    .Select(game => new
                    {
                        Id = game.Id,
                        Title = game.Name,
                        Developer = game.Developer.Name,
                        Tags = string.Join(", ", game.GameTags.Select(t => t.Tag.Name).ToList()),
                        Players = game.Purchases.Count
                    })
                    .OrderByDescending(x => x.Players)
                    .ThenBy(x => x.Id)
                    .ToList(),
                    TotalPlayers = g.Games.Sum(x => x.Purchases.Count())
                })
                .OrderByDescending(x => x.TotalPlayers)
                .ThenBy(x => x.Id)
                .ToList();

            var res = JsonConvert.SerializeObject(genres, Formatting.Indented);

            return res;
        }

		public static string ExportUserPurchasesByType(VaporStoreDbContext context, string storeType)
		{
            var serializer = new XmlSerializer(typeof(ExportUserDto[]), new XmlRootAttribute("Users"));

            var purchaseType = Enum.Parse<PurchaseType>(storeType);

            var users = context.Users
                .ToList()
                .Where(u => u.Cards.Any(c => c.Purchases.Count > 0))
                .Select(u => new ExportUserDto()
                {
                    Username = u.Username,
                    Purchases = (System.Collections.Generic.List<ExportPurchaseDto>)u.Cards
                        .SelectMany(c => c.Purchases
                        .Where(p => p.Type == purchaseType)
                        .Select(p => new ExportPurchaseDto()
                            {
                                Card = p.Card.Number,
                                Cvc = p.Card.Cvc,
                                Date = p.Date.ToString("yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                                Game = new ExportGameDto()
                                {
                                    Title = p.Game.Name,
                                    Genre = p.Game.Genre.Name,
                                    Price = p.Game.Price
                                }
                            }))
                        .OrderBy(x => x.Date)
                        .ToList(),
                    TotalSpent = u.Cards
                        .SelectMany(c => c.Purchases
                            .Where(p => p.Type == purchaseType)
                            .Select(p => p.Game.Price))
                        .Sum()
                })
                .Where(u => u.Purchases.Count > 0)
                .OrderByDescending(u => u.TotalSpent)
                .ThenBy(u => u.Username)
                .ToArray();


            using StringWriter strr = new StringWriter();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            serializer.Serialize(strr, users, ns);

            return strr.ToString().TrimEnd();
        }
	}
}