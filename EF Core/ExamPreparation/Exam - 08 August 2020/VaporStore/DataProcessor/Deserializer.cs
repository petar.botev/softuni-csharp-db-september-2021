﻿namespace VaporStore.DataProcessor
{
	using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;
    using Data;
    using Newtonsoft.Json;
    using VaporStore.Data.Models;
    using VaporStore.Data.Models.Enums;
    using VaporStore.DataProcessor.Dto.Import;

    public static class Deserializer
	{
		public static string ImportGames(VaporStoreDbContext context, string jsonString)
		{
			var games = JsonConvert.DeserializeObject<ImportGamesDto[]>(jsonString);

			StringBuilder strb = new StringBuilder();

			List<Game> allGames = new List<Game>();
			List<Developer> developers = new List<Developer>();
			List<Genre> genres = new List<Genre>();
			List<Tag> tags = new List<Tag>();

			foreach (var game in games)
            {
                if (!IsValid(game))
                {
					strb.AppendLine("Invalid Data");
					continue;
                }

				var isReleaseDateValid = DateTime.TryParseExact(game.ReleaseDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime releaseDate);

				if (!isReleaseDateValid)
				{
					strb.AppendLine("Invalid Data");
					continue;
				}

				var newGame = new Game
				{
					Name = game.Name,
					Price = game.Price,
					ReleaseDate = releaseDate,
				};

				var developer = developers.FirstOrDefault(d => d.Name == game.Developer);

				if (developer == null)
                {
					var newDeveloper = new Developer() { Name = game.Developer };
					developers.Add(newDeveloper);

					newGame.Developer = newDeveloper;
				}
                else
                {
					newGame.Developer = developer;
                }

				var genre = genres.FirstOrDefault(g => g.Name == game.Genre);

                if (genre == null)
                {
					var newGenre = new Genre() { Name = game.Genre };
					genres.Add(newGenre);

					newGame.Genre = newGenre;
				}
                else
                {
					newGame.Genre = genre;
                }

                foreach (var tag in game.Tags)
                {
					var currTag = tags.FirstOrDefault(t => t.Name == tag);

                    if (currTag == null)
                    {
						var newTag = new Tag { Name = tag };

						tags.Add(newTag);

						newGame.GameTags.Add(new GameTag
						{
							Game = newGame,
							Tag = newTag
						});
                    }
                    else
                    {
						newGame.GameTags.Add(new GameTag
						{
							Game = newGame,
							Tag = currTag
						});
					}
                }

				allGames.Add(newGame);
				strb.AppendLine($"Added {newGame.Name} ({newGame.Genre.Name}) with {newGame.GameTags.Count} tags");
            }

			context.AddRange(allGames);
			context.SaveChanges();

            //StringBuilder strb2 = new StringBuilder();

            //strb2.AppendLine($"Games: {allGames.Count}, Developers: {developers.Count}, Genres: {genres.Count}, Tags: {tags.Count}");
            //Console.WriteLine(strb2.ToString().TrimEnd());
            return strb.ToString().TrimEnd();
		}

		public static string ImportUsers(VaporStoreDbContext context, string jsonString)
		{
			var users = JsonConvert.DeserializeObject<ImportUsersDto[]>(jsonString);

			StringBuilder strb = new StringBuilder();

			
			var allUsers = new List<User>();

			foreach (var user in users)
            {
                if (!IsValid(user))
                {
					strb.AppendLine("Invalid Data");
					continue;
                }

				var newUser = new User
				{
					FullName = user.FullName,
					Username = user.Username,
					Email = user.Email,
					Age = user.Age
				};

				bool isCardValid = true;

				var cards = new List<Card>();

				foreach (var card in user.Cards)
                {
                    if (!IsValid(card))
                    {
						strb.AppendLine("Invalid Data");
						isCardValid = false;
						break;
					}

					var newCard = new Card
					{
						Number = card.Number,
						Cvc = card.Cvc,
						Type = card.Type
					};

					cards.Add(newCard);
                }

                if (!isCardValid)
                {
					strb.AppendLine("Invalid Data");
					continue;
				}

				newUser.Cards = cards;

				allUsers.Add(newUser);

				strb.AppendLine($"Imported {newUser.Username} with {newUser.Cards.Count} cards");
            }

			context.AddRange(allUsers);
			context.SaveChanges();
			//StringBuilder strb2 = new StringBuilder();

			//strb2.AppendLine($"Users: {allUsers.Count}, Cards: {allUsers.Sum(u => u.Cards.Count)}");
			//Console.WriteLine(strb2.ToString().TrimEnd());

			return strb.ToString().TrimEnd();
		}

		public static string ImportPurchases(VaporStoreDbContext context, string xmlString)
		{
			var serializer = new XmlSerializer(typeof(ImportPurchases[]), new XmlRootAttribute("Purchases"));

			using StringReader strr = new StringReader(xmlString);
			var purchases = (ImportPurchases[])serializer.Deserialize(strr);

			var strb = new StringBuilder();
			var allPurchases = new List<Purchase>();

            foreach (var purchase in purchases)
            {
                if (!IsValid(purchase))
                {
					strb.AppendLine("Invalid Data");
					continue;
				}

				var card = context.Cards.FirstOrDefault(c => c.Number == purchase.CardNumber);

                if (card == null)
                {
					strb.AppendLine("Invalid Data");
					continue;
				}

				var game = context.Games.FirstOrDefault(g => g.Name == purchase.GameTitle);

                if (game == null)
                {
					strb.AppendLine("Invalid Data");
					continue;
				}

				var isPTypeValid = Enum.TryParse(typeof(PurchaseType), purchase.Type, out object pType);

                if (!isPTypeValid)
                {
					strb.AppendLine("Invalid Data");
					continue;
				}

				var isDateValid = DateTime.TryParseExact(purchase.Date, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date);

                if (!isDateValid)
                {
					strb.AppendLine("Invalid Data");
					continue;
				}

				var newPurchase = new Purchase
				{
					Game = game,
					Type = (PurchaseType)pType,
					ProductKey = purchase.ProductKey,
					Card = card,
					Date = date
				};

				allPurchases.Add(newPurchase);

				strb.AppendLine($"Imported {newPurchase.Game.Name} for {newPurchase.Card.User.Username}");
            }

			context.AddRange(allPurchases);
			context.SaveChanges();

			return strb.ToString().TrimEnd();
		}

		private static bool IsValid(object dto)
		{
			var validationContext = new ValidationContext(dto);
			var validationResult = new List<ValidationResult>();

			return Validator.TryValidateObject(dto, validationContext, validationResult, true);
		}
	}
}