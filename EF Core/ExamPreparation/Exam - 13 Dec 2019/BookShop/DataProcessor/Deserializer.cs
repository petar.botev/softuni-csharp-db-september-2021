﻿namespace BookShop.DataProcessor
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;
    using BookShop.Data.Models;
    using BookShop.Data.Models.Enums;
    using BookShop.DataProcessor.ImportDto;
    using Data;
    using Newtonsoft.Json;
    using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;

    public class Deserializer
    {
        private const string ErrorMessage = "Invalid data!";

        private const string SuccessfullyImportedBook
            = "Successfully imported book {0} for {1:F2}.";

        private const string SuccessfullyImportedAuthor
            = "Successfully imported author - {0} with {1} books.";

        public static string ImportBooks(BookShopContext context, string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ImportBookDto[]), new XmlRootAttribute("Books"));

            StringReader strr = new StringReader(xmlString);
            var bookDtos = (ImportBookDto[])serializer.Deserialize(strr);

            List<Book> books = new List<Book>();

            StringBuilder strb = new StringBuilder();

            foreach (var bookDto in bookDtos)
            {
                if (!IsValid(bookDto))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var isDateValid = DateTime.TryParseExact(bookDto.PublishedOn, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime newDatetime);

                if (!isDateValid)
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var isGenreValid = Enum.TryParse<Genre>(bookDto.Genre, out Genre newGenre);

                if (!isGenreValid || (int)newGenre > 3 || (int)newGenre < 1)
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var book = new Book
                {
                    Name = bookDto.Name,
                    Genre = newGenre,
                    Price = bookDto.Price,
                    Pages = bookDto.Pages,
                    PublishedOn = newDatetime
                };

                books.Add(book);

                strb.AppendLine(string.Format(SuccessfullyImportedBook, book.Name, book.Price));
            }

            context.AddRange(books);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }


        public static string ImportAuthors(BookShopContext context, string jsonString)
        {
            var authorDtos = JsonConvert.DeserializeObject<ImportAuthorDto[]>(jsonString);

            StringBuilder strb = new StringBuilder();

            List<Author> authors = new List<Author>();

            foreach (var authorDto in authorDtos)
            {
                if (!IsValid(authorDto))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                if (authors.Select(a => a.Email).Contains(authorDto.Email))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var author = new Author
                {
                    FirstName = authorDto.FirstName,
                    LastName = authorDto.LastName,
                    Email = authorDto.Email,
                    Phone = authorDto.Phone,
                };

                List<ImportAuthorBookDto> bookList = new List<ImportAuthorBookDto>();
                foreach (var book in authorDto.Books)
                {
                    if (!IsValid(book))
                    {
                        strb.AppendLine(ErrorMessage);
                        continue;
                    }
                    var currBook = context.Books.FirstOrDefault(x => x.Id == book.Id);

                    if (currBook == null)
                    {
                        continue;
                    }

                    bookList.Add(book);
                    author.AuthorsBooks.Add(new AuthorBook
                    {
                        Author = author,
                        Book = currBook
                    });
                }

                if (bookList.Count == 0)
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                authors.Add(author);
                strb.AppendLine(string.Format(SuccessfullyImportedAuthor, $"{author.FirstName} {author.LastName}", author.AuthorsBooks.Count));
            }

            context.AddRange(authors);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }


        private static bool IsValid(object dto)
        {
            var validationContext = new ValidationContext(dto);
            var validationResult = new List<ValidationResult>();

            return Validator.TryValidateObject(dto, validationContext, validationResult, true);
        }
    }
}