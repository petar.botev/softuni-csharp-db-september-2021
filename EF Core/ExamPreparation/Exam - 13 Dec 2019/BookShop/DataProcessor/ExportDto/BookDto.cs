﻿using System.Xml.Serialization;

namespace BookShop.DataProcessor.ExportDto
{
    [XmlType("Book")]
    public class BookDto
    {
        [XmlAttribute("Pages")]
        public string Pages { get; set; }
        public string Name{ get; set; }
        public string Date { get; set; }
    }
}
