﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace BookShop.DataProcessor.ImportDto
{
    public class ImportAuthorDto
    {
        [Required]
        [MinLength(3)]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]{3}-[0-9]{3}-[0-9]{4}$")]
        public string Phone { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public ImportAuthorBookDto[] Books { get; set; }
        //[Required]
        //[StringLength(20, MinimumLength = 3)]
        //public string FirstName { get; set; }

        //[Required]
        //[StringLength(20, MinimumLength = 3)]
        //public string LastName { get; set; }

        //[Required]
        //[RegularExpression(@"^\d{3}-\d{3}-\d{4}$")]
        //public string Phone { get; set; }

        //[Required]
        //[EmailAddress]
        //public string Email { get; set; }

        //public List<ImportAuthorBookDto> Books { get; set; }
    }
}
