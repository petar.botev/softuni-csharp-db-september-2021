﻿namespace BookShop.DataProcessor
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using BookShop.Data.Models.Enums;
    using BookShop.DataProcessor.ExportDto;
    using Data;
    using Newtonsoft.Json;
    using Formatting = Newtonsoft.Json.Formatting;

    public class Serializer
    {
        public static string ExportMostCraziestAuthors(BookShopContext context)
        {
            var authors = context
                 .Authors
                 .Select(a => new
                 {
                     AuthorName = $"{a.FirstName} {a.LastName}",
                     Books = a.AuthorsBooks
                         .Select(ab => new
                         {
                             BookName = ab.Book.Name,
                             BookPrice = ab.Book.Price
                         })
                         .OrderByDescending(ab => ab.BookPrice)
                         .Select(b => new
                         {
                             BookName = b.BookName,
                             BookPrice = b.BookPrice.ToString("f2")
                         })
                         .ToArray()
                 })
                 .ToArray()
                 .OrderByDescending(a => a.Books.Length)
                 .ThenBy(a => a.AuthorName)
                 .ToArray();

            var authorsJson = JsonConvert.SerializeObject(authors, Formatting.Indented);

            return authorsJson;
        }

        public static string ExportOldestBooks(BookShopContext context, DateTime date)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BookDto[]), new XmlRootAttribute("Books"));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            var bookDtos = context.Books
                .ToList()
                .Where(b => b.Genre == Genre.Science && b.PublishedOn < date)
                .OrderByDescending(b => b.Pages)
                .ThenByDescending(b => b.PublishedOn)
                .Select(b => new BookDto
                {
                    Pages = b.Pages.ToString(),
                    Name = b.Name,
                    Date = b.PublishedOn.ToString("d", CultureInfo.InvariantCulture)
                })
                .Take(10)
                .ToArray();

            StringWriter strr = new StringWriter();
            serializer.Serialize(strr, bookDtos, ns);

            return strr.ToString().TrimEnd();
        }
    }
}