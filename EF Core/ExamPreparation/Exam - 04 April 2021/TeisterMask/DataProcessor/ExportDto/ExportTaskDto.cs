﻿using System.Xml.Serialization;
using TeisterMask.Data.Models.Enums;

namespace TeisterMask.DataProcessor.ExportDto
{
    [XmlType("Task")]
    public class ExportTaskDto
    {

        public string Name { get; set; }

        [XmlElement("Label")]
        public LabelType LabelType { get; set; }
    }
}
