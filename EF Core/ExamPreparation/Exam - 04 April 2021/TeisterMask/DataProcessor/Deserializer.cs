﻿namespace TeisterMask.DataProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Xml.Serialization;
    using Data;
    using TeisterMask.DataProcessor.ImportDto;
    using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;
    using TeisterMask.Data.Models;
    using System.Text;
    using System.Globalization;
    using TeisterMask.Data.Models.Enums;
    using Newtonsoft.Json;

    public class Deserializer
    {
        private const string ErrorMessage = "Invalid data!";

        private const string SuccessfullyImportedProject
            = "Successfully imported project - {0} with {1} tasks.";

        private const string SuccessfullyImportedEmployee
            = "Successfully imported employee - {0} with {1} tasks.";

        public static string ImportProjects(TeisterMaskContext context, string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ProjectDto[]), new XmlRootAttribute("Projects"));

            using StringReader strr = new StringReader(xmlString);
            var projectDto = (ProjectDto[])serializer.Deserialize(strr);

            List<Project> projList = new List<Project>();
            
            StringBuilder strb = new StringBuilder();

            foreach (var project in projectDto)
            {
                if (!IsValid(project))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                var isOpenDateTrue = DateTime.TryParseExact(project.OpenDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime openDate);

                if (!isOpenDateTrue)
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                DateTime? newDueDate = null;
                var isDueDateTrue = DateTime.TryParseExact(project.DueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dueDate);

                if (isDueDateTrue)
                {
                    newDueDate = dueDate;
                }
                
                var taskObjList = new List<Task>();
                foreach (var task in project.Tasks)
                {
                    if (!IsValid(task))
                    {
                        strb.AppendLine(ErrorMessage);
                        continue;
                    }

                    var isTaskOpenDateTrue = DateTime.TryParseExact(task.OpenDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime taskOpenDate);

                    if (!isTaskOpenDateTrue)
                    {
                        strb.AppendLine(ErrorMessage);
                        continue;
                    }

                    var isTaskDueDateTrue = DateTime.TryParseExact(task.DueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime taskDueDate);

                    if (!isTaskDueDateTrue)
                    {
                        strb.AppendLine(ErrorMessage);
                        continue;
                    }

                    if (taskOpenDate < openDate || (taskDueDate > newDueDate && newDueDate != null))
                    {
                        strb.AppendLine(ErrorMessage);
                        continue;
                    }

                    var newTask = new Task
                    {
                        Name = task.Name,
                        OpenDate = taskOpenDate,
                        DueDate = taskDueDate,
                        ExecutionType = (ExecutionType)task.ExecutionType,
                        LabelType = (LabelType)task.LabelType
                    };

                    taskObjList.Add(newTask);
                }

                var newProject = new Project
                {
                    Name = project.Name,
                    OpenDate = openDate,
                    DueDate = newDueDate,
                    Tasks = taskObjList
                };

                projList.Add(newProject);

                strb.AppendLine(string.Format(SuccessfullyImportedProject, newProject.Name, newProject.Tasks.Count));
            }

            context.AddRange(projList);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }

        public static string ImportEmployees(TeisterMaskContext context, string jsonString)
        {
            var employeeDto = JsonConvert.DeserializeObject<List<EmployeesDto>>(jsonString);

            StringBuilder strb = new StringBuilder();
            var employees = new List<Employee>();

            foreach (var employee in employeeDto)
            {
                var tasks = new List<EmployeeTask>();

                if (!IsValid(employee))
                {
                    strb.AppendLine(ErrorMessage);
                    continue;
                }

                foreach (var task in employee.EmployeesTasks.Distinct())
                {
                    var currTask = context.Tasks
                        .FirstOrDefault(t => t.Id == task);

                    if (currTask == null)
                    {
                        strb.AppendLine(ErrorMessage);
                        continue;
                    }

                    var et = new EmployeeTask
                    {
                        Task = currTask
                    };

                    tasks.Add(et);
                }

                var newEmployee = new Employee
                {
                    Username = employee.Username,
                    Email = employee.Email,
                    Phone = employee.Phone,
                    EmployeesTasks = tasks
                };

                employees.Add(newEmployee);

                strb.AppendLine(string.Format(SuccessfullyImportedEmployee, newEmployee.Username, newEmployee.EmployeesTasks.Count));
            }

            context.AddRange(employees);
            context.SaveChanges();

            return strb.ToString().TrimEnd();
        }

        private static bool IsValid(object dto)
        {
            var validationContext = new ValidationContext(dto);
            var validationResult = new List<ValidationResult>();

            return Validator.TryValidateObject(dto, validationContext, validationResult, true);
        }
    }
}