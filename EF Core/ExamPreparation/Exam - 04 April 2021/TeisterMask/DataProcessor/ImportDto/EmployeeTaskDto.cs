﻿using System.ComponentModel.DataAnnotations;

namespace TeisterMask.DataProcessor.ImportDto
{
    public class EmployeeTaskDto
    {
        [Key]
        public int Id { get; set; }
    }
}
