﻿namespace TeisterMask.Data
{
    using Microsoft.EntityFrameworkCore;
    using TeisterMask.Data.Models;

    public class TeisterMaskContext : DbContext
    {
        public TeisterMaskContext() { }

        public TeisterMaskContext(DbContextOptions options)
            : base(options) { }


        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeTask> EmployeesTasks { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseSqlServer(Configuration.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Employee>(e =>
            //{
            //    e.HasMany(x => x.EmployeesTasks)
            //    .WithOne(x => x.Employee);
            //});

            //modelBuilder.Entity<Task>(e =>
            //{
            //    e.HasMany(t => t.EmployeesTasks)
            //    .WithOne(et => et.Task);
            //});

            modelBuilder.Entity<EmployeeTask>(et =>
            {
                et.HasOne(e => e.Employee)
                .WithMany(e => e.EmployeesTasks);

                et.HasOne(et => et.Task)
                .WithMany(et => et.EmployeesTasks);
            });

            modelBuilder.Entity<EmployeeTask>(e =>
            {
                e.HasKey(et => new { et.EmployeeId, et.TaskId });
            });
        }
    }
}