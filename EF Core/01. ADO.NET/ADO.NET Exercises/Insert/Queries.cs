﻿using Microsoft.Data.SqlClient;
using System;

namespace Exercise_1
{
    public class Queries
    {
        public void InsertQuery()
        {
            using (var connection = new SqlConnection("Server=(LocalDb)\\MySqlDb;Integrated Security=true;Database=SoftUni"))
            {
                connection.Open();

                string query = @"INSERT INTO Departments(Name, ManagerID)
                                 VALUES('Dep1', 100)
	                             , ('Dep2', 101)
	                             , ('Dep3', 102)";

                string query2 = @"SELECT *
                                  FROM Departments";

                SqlCommand command = new SqlCommand(query, connection);
                SqlCommand command2 = new SqlCommand(query2, connection);

                int res = command.ExecuteNonQuery();
                Console.WriteLine(res);

                SqlDataReader reader2 = command2.ExecuteReader();

                while (reader2.Read())
                {
                    Console.WriteLine(reader2[1]);
                }
            }
        }

        public void UpdateQuery()
        {
            using (var connection = new SqlConnection("Server=(LocalDb)\\MySqlDb;Integrated Security=true;Database=SoftUni"))
            {
                connection.Open();

                string query = @"UPDATE Departments
                                 SET Name = Name + 'Updated'
                                 WHERE DepartmentID IN(17, 18, 19)";

                SqlCommand command1 = new SqlCommand(query, connection);
                int affectedRows = command1.ExecuteNonQuery();
                Console.WriteLine(affectedRows);

                string query2 = @"SELECT *
                                FROM Departments";

                SqlCommand command2 = new SqlCommand(query2, connection);

                var reader = command2.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine(reader[1]);
                }
            }
        }

        public void DeleteQuery()
        {
            using (var connection = new SqlConnection("Server=(LocalDb)\\MySqlDb;Integrated Security=true;Database=SoftUni"))
            {
                connection.Open();

                string query1 = @"DELETE FROM Departments
                                  WHERE DepartmentID IN (17,18,19)";

                SqlCommand command1 = new SqlCommand(query1, connection);
                int res = command1.ExecuteNonQuery();
                Console.WriteLine(res);

                string query2 = @"SELECT *
                                  FROM Departments";

                SqlCommand command2 = new SqlCommand(query2, connection);

                var res2 = command2.ExecuteReader();

                while (res2.Read())
                {
                    Console.WriteLine(res2[1]);
                }
            }
        }
    }
}
