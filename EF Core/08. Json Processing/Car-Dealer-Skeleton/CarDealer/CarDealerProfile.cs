﻿using AutoMapper;
using CarDealer.DTO;
using CarDealer.Models;
using System.Linq;

namespace CarDealer
{
    public class CarDealerProfile : Profile
    {
        public CarDealerProfile()
        {
            this.CreateMap<PartsDTO, Part>();
            this.CreateMap<CustomerDTO, Customer>();
            this.CreateMap<SaleDTO, Sale>();
            this.CreateMap<Customer, CustomerDTO>()
                .ForMember(x => x.BirthDate, y => y.MapFrom(z => z.BirthDate.ToString("dd/MM/yyyy")));
            this.CreateMap<Supplier, SupplierDTO>()
                .ForMember(x => x.PartsCount, y => y.MapFrom(z => z.Parts.Count));
            this.CreateMap<Part, PartsDTO>();
                
        }
    }
}
