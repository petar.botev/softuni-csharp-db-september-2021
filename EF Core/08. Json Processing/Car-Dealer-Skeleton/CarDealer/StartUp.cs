﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using CarDealer.Data;
using CarDealer.DTO;
using CarDealer.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CarDealer
{
    public class StartUp
    {
        private static IMapper mapper;
        public static void Main(string[] args)
        {
            var context = new CarDealerContext();
            var inputJson = File.ReadAllText(@"..\..\..\Datasets\cars.json");
            //Console.WriteLine(GetSalesWithAppliedDiscount(context));
            Console.WriteLine(ImportCars(context, inputJson));
        }

        public static string ImportCars(CarDealerContext context, string inputJson)
        {
            var cars = JsonConvert.DeserializeObject<List<CarsImport>>(inputJson);

            var allCars = new List<Car>();
            var allCarsParts = new List<PartCar>();

            foreach (var carDTO in cars)
            {
                var newCar = new Car
                {
                    Make = carDTO.Make,
                    Model = carDTO.Model,
                    TravelledDistance = carDTO.TravelledDistance,
                };
                
                foreach (var part in carDTO.PartsId.Distinct())
                {
                    var newPart = new PartCar()
                    {
                        Car = newCar,
                        PartId = part
                    };
                    allCarsParts.Add(newPart);
                }

                allCars.Add(newCar);
            }

            context.Cars.AddRange(allCars);
            context.PartCars.AddRange(allCarsParts);

            context.SaveChanges();
            
            return $"Successfully imported {cars.Count}.";
        }

        //12. Export Sales With Applied Discount
        public static string GetSalesWithAppliedDiscount(CarDealerContext context)
        {
            var sales = context.Sales
                .Select(s => new
                {
                    car = new
                    {
                        Make = s.Car.Make,
                        Model = s.Car.Model,
                        TravelledDistance = s.Car.TravelledDistance
                    },
                    customerName = s.Customer.Name,
                    Discount = s.Discount.ToString("f2"),
                    price = s.Car.PartCars.Sum(pc => pc.Part.Price).ToString("f2"),
                    priceWithDiscount = (s.Car.PartCars.Sum(pc => pc.Part.Price) - s.Car.PartCars.Sum(pc => pc.Part.Price) * (s.Discount/100)).ToString("f2")
                })
                .Take(10)
                .ToList();

            var json = JsonConvert.SerializeObject(sales, Formatting.Indented);

            return json;
        }

        //11. Export Total Sales By Customer
        public static string GetTotalSalesByCustomer(CarDealerContext context)
        {
            var customers = context.Customers
                .Where(c => c.Sales.Count > 0)
                .Select(c => new
                {
                    fullName = c.Name,
                    boughtCars = c.Sales.Count,
                    spentMoney = c.Sales.Sum(s => s.Car.PartCars.Sum(pc => pc.Part.Price))

                })
                .OrderByDescending(c => c.spentMoney)
                .ThenByDescending(c => c.boughtCars)
                .ToList();

            var json = JsonConvert.SerializeObject(customers, Formatting.Indented);

            return json;
        }

        //10. Export Cars With Their List Of Parts
        public static string GetCarsWithTheirListOfParts(CarDealerContext context)
        {
            var cars = context.Cars
                .Select(c => new
                {
                    car = new
                    {
                        c.Make,
                        c.Model,
                        c.TravelledDistance
                    },
                    parts = c.PartCars.Select(pc => new
                    {
                        pc.Part.Name,
                        Price = pc.Part.Price.ToString("f2")
                    }).ToList()
                })
                
                .ToList();

            var json = JsonConvert.SerializeObject(cars, Formatting.Indented);

            return json;
        }

        //09. Export Local Suppliers
        public static string GetLocalSuppliers(CarDealerContext context)
        {
            InitializeAutomapper();
            var suppliers = context.Suppliers
                .Where(x => x.IsImporter == false)
                .ToList();

            var suppliersDTO = mapper.Map<List<SupplierDTO>>(suppliers);

            var json = JsonConvert.SerializeObject(suppliersDTO, Formatting.Indented);

            return json;
        }

        //08. Export Cars From Make Toyota
        public static string GetCarsFromMakeToyota(CarDealerContext context)
        {
            InitializeAutomapper();
            var cars = context.Cars
                .Where(x => x.Make == "Toyota")
                .OrderBy(x => x.Model)
                .ThenByDescending(x => x.TravelledDistance)
                .ToList();

            var carDTO = mapper.Map<List<CarsDTO>>(cars);
            var json = JsonConvert.SerializeObject(carDTO, Formatting.Indented);

            return json;
        }

        //07. Export Ordered Customers
        public static string GetOrderedCustomers(CarDealerContext context)
        {
            InitializeAutomapper();
            var customers = context.Customers
                .OrderBy(c => c.BirthDate)
                .ThenBy(c => c.IsYoungDriver)
                .ToList();
            var customersDTOs = mapper.Map<List<CustomerDTO>>(customers);

            var json = JsonConvert.SerializeObject(customersDTOs, Formatting.Indented);

            return json;
        }

        //06. Import Sales
        public static string ImportSales(CarDealerContext context, string inputJson)
        {
            InitializeAutomapper();
            var saleDTOs = JsonConvert.DeserializeObject<List<SalesImport>>(inputJson);

            var sales = mapper.Map<List<Sale>>(saleDTOs);

            context.AddRange(sales);
            var res = context.SaveChanges();

            return $"Successfully imported {res}.";
        }

        //05. Import Customers
        public static string ImportCustomers(CarDealerContext context, string inputJson)
        {
            InitializeAutomapper();
            var customerDTO = JsonConvert.DeserializeObject<List<CustomerImport>>(inputJson);

            var customers = mapper.Map<List<Customer>>(customerDTO);

            context.AddRange(customers);
            var res = context.SaveChanges();
            return $"Successfully imported {res}.";
        }


        //04. Import Cars
        //public static string ImportCars(CarDealerContext context, string inputJson)
        //{
        //    var cars = JsonConvert.DeserializeObject<List<Car>>(inputJson);

        //    context.AddRange(cars);
        //    var res = context.SaveChanges();

        //    return $"Successfully imported {res}.";
        //}

        //03. Import Parts
        public static string ImportParts(CarDealerContext context, string inputJson)
        {
            InitializeAutomapper();
            var suppliers = context.Suppliers
                .Select(x => x.Id)
                .ToList();

            var partsDTO = JsonConvert.DeserializeObject<List<PartsImport>>(inputJson)
                .Where(x => suppliers.Contains(x.SupplierId))
                .ToList();

            var parts = mapper.Map<List<Part>>(partsDTO);

            context.AddRange(parts);
          
            var res = context.SaveChanges();

            return $"Successfully imported {res}.";
        }

        //02. Import Data
        public static string ImportSuppliers(CarDealerContext context, string inputJson)
        {
            InitializeAutomapper();
            var suppliersDTO = JsonConvert.DeserializeObject<List<SupplierImport>>(inputJson);
            var suppliers = mapper.Map<List<Supplier>>(suppliersDTO);

            context.AddRange(suppliers);
            context.SaveChanges();
            var res = $"Successfully imported {suppliers.Count}.";

            return res;
        }

        private static void InitializeAutomapper()
        {
            var config = new MapperConfiguration(cfg =>
                        cfg.AddProfile<CarDealerProfile>()
                );
            mapper = config.CreateMapper();
        }
    }
}