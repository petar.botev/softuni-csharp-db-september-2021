﻿namespace CarDealer.DTO
{
    public class SaleDTO
    {
        public int Id { get; set; }

        public decimal Discount { get; set; }

        public int? CarId { get; set; }

        public int? CustomerId { get; set; }
        
    }
}
