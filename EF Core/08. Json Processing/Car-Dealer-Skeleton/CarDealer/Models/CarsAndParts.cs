﻿using CarDealer.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarDealer.Models
{
    public class CarsAndParts
    {
        public CarsDTO Car { get; set; }

        public List<Part> Parts { get; set; }
    }
}
