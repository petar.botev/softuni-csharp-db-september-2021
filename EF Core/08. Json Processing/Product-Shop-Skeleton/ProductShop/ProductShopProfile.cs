﻿using AutoMapper;
using ProductShop.Models;
using ProductShop.DTOs;

namespace ProductShop
{
    public class ProductShopProfile : Profile
    {
        public ProductShopProfile()
        {
            this.CreateMap<Product, ProductViewModel>()
                .ForMember(x => x.SellerFullName, y => y.MapFrom(z => $"{z.Seller.FirstName} {z.Seller.LastName}"));
        }
    }
}
