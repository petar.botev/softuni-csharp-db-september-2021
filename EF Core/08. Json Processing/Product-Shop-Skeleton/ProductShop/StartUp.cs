﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ProductShop.Data;
using ProductShop.DTOs;
using ProductShop.Models;
using Microsoft.EntityFrameworkCore;


namespace ProductShop
{
    public class StartUp
    {
        private static IMapper mapper;

        public static void Main(string[] args)
        {
            var db = new ProductShopContext();
            //db.Database.EnsureDeleted();
            //db.Database.EnsureCreated();
            //var jsonString1 = File.ReadAllText(@"..\..\..\Datasets\users.json");

            //var jsonString2 = File.ReadAllText(@"..\..\..\Datasets\products.json");

            //var jsonString3 = File.ReadAllText(@"..\..\..\Datasets\categories.json");

            //var jsonString4 = File.ReadAllText(@"..\..\..\Datasets\categories-products.json");

            //Console.WriteLine(ImportUsers(db, jsonString1));
            //Console.WriteLine(ImportProducts(db, jsonString2));
            //Console.WriteLine(ImportCategories(db, jsonString3));
            //Console.WriteLine(ImportCategoryProducts(db, jsonString4));

            Console.WriteLine(GetUsersWithProducts(db));
        }

        //09. Export Users and Products
        public static string GetUsersWithProducts(ProductShopContext context)
        {
            var users = context.Users
                .Include(x => x.ProductsSold)
                .ToList()
                .Where(u => u.ProductsSold.Any(ps => ps.BuyerId != null))
                .OrderByDescending(u => u.ProductsSold.Where(x => x.BuyerId != null).Count())
                .Select(u => new
                {
                    firstName = u.FirstName,
                    lastName = u.LastName,
                    age = u.Age,
                    soldProducts = new
                    {
                        count = u.ProductsSold.Where(p => p.BuyerId != null).Count(),
                        products = u.ProductsSold.Where(p => p.BuyerId != null).Select(s => new
                        {
                            name = s.Name,
                            price = s.Price
                        })
                    }
                })
                .ToList();

            var usersCount = context.Users
                .Where(u => u.ProductsSold.Any(ps => ps.BuyerId != null))
                .Count();

            var finalObject = new
            {
                usersCount = usersCount,
                users = users
            };

            var jsonSettings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
            };

            var json = JsonConvert.SerializeObject(finalObject, Formatting.Indented, jsonSettings);

            return json;
        }

        //08. Export Categories By Products Count
        public static string GetCategoriesByProductsCount(ProductShopContext context)
        {
            var categories = context.Categories
                .OrderByDescending(c => c.CategoryProducts.Count)
                .Select(c => new
                {
                    category = c.Name,
                    productsCount = c.CategoryProducts.Count,
                    averagePrice = c.CategoryProducts.Average(cp => cp.Product.Price).ToString("f2"),
                    totalRevenue = c.CategoryProducts.Sum(cp => cp.Product.Price).ToString("f2"),
                })
                .ToList();

            var json = JsonConvert.SerializeObject(categories, Formatting.Indented);

            return json;
        }


        //07. Export Sold Products
        public static string GetSoldProducts(ProductShopContext context)
        {
            var users = context.Users
                .Where(x => x.ProductsSold.Any(p => p.BuyerId != null))
                .Select(u => new
                {
                    firstName = u.FirstName,
                    lastName = u.LastName,
                    soldProducts = u.ProductsSold.Where(x => x.BuyerId != null).Select(ps => new
                    {
                        name = ps.Name,
                        price = ps.Price,
                        buyerFirstName = ps.Buyer.FirstName,
                        buyerLastName = ps.Buyer.LastName
                    }).ToList()
                })
                .OrderBy(x => x.lastName)
                .ThenBy(x => x.firstName)
                .ToList();

            var json = JsonConvert.SerializeObject(users, Formatting.Indented);

            return json;
        }

        //06. Export Products In Range
        public static string GetProductsInRange(ProductShopContext context)
        {
            var products = context.Products
                .Where(p => p.Price >= 500 && p.Price <= 1000)
                .OrderBy(p => p.Price)
                .Select(p => new
                {
                   p.Name,
                   p.Price,
                   Seller= $"{p.Seller.FirstName} {p.Seller.LastName}",
                })
                .ToList();

            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            string json = JsonConvert.SerializeObject(products, new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            });
            //var json = JsonConvert.SerializeObject(products, Formatting.Indented, serial.Serialize);

            return json;
        }

        //05. Import Categories and Products
        public static string ImportCategoryProducts(ProductShopContext context, string inputJson)
        {
            var categoriesProducts = JsonConvert.DeserializeObject<List<CategoryProduct>>(inputJson);

            context.CategoryProducts.AddRange(categoriesProducts);
            var result = context.SaveChanges();

            return $"Successfully imported {result}";
        }

        //04. Import Categories
        public static string ImportCategories(ProductShopContext context, string inputJson)
        {
            //var jsonSettings = new JsonSerializerSettings()
            //{
            //    NullValueHandling = NullValueHandling.Ignore,
            //    MissingMemberHandling = MissingMemberHandling.Ignore
            //};

            var categories = JsonConvert.DeserializeObject<List<Category>>(inputJson)
                .Where(x => x.Name != null);

            context.Categories.AddRange(categories);

            int result = context.SaveChanges();

            return $"Successfully imported {result}";
        }

        //03. Import Products
        public static string ImportProducts(ProductShopContext context, string inputJson)
        {
            var products = JsonConvert.DeserializeObject<List<Product>>(inputJson);

            context.Products.AddRange(products);

            int result = context.SaveChanges();

            return $"Successfully imported {result}";
        }

        //02. Import Users
        public static string ImportUsers(ProductShopContext context, string inputJson)
        {
            InitializeAutomapper();
            var usersDto = JsonConvert.DeserializeObject<List<UserImport>>(inputJson);

            var users = mapper.Map<List<User>>(usersDto);

            context.Users.AddRange(users);

            var result =  $"Successfully imported {context.SaveChanges()}";

            return result;
        }

        private static void InitializeAutomapper()
        {
            var config = new MapperConfiguration(cfg =>
                    cfg.AddProfile<ProductShopProfile>()
                );
            mapper = config.CreateMapper();
        }
    }
}